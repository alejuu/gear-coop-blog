<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache


/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'gearcoopblogdev' );

/** MySQL database username */
define( 'DB_USER', 'wp' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wp' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+aR:]}jO}5U#!Ef$l@f9H!Sp$YD,#A>f]}Sl2{2Vex#jyLvu-$[N]R_r}Nz*;51k');
define('SECURE_AUTH_KEY',  'g}mXk{rab5S@FxAlyF$P|{4|zKIcV.k5#&^3j0M)8<7(Z|ubGny}|4x{ov2%|@N$');
define('LOGGED_IN_KEY',    'l^oTz;nMkaQ^,X|QHeem_OKQ-?nb|XoG6C5zyca#KvbZpEldZwsOk!A|^.w`G>K4');
define('NONCE_KEY',        'G5of!<WT7))#4h*~?u^/z+-^6:28M8JN4qM.VtSaUgaBGr.(mnPgdq3=+ZqD91Fh');
define('AUTH_SALT',        'z;Ccefq$>mUEu+]~-vmU49<TOL;A0K{ic_1Zc/CRE/dDgTYd*EO(s$B}k.($2t>6');
define('SECURE_AUTH_SALT', '?`ufSDYpKj!-sRB[$ ZZ9L[<~#Y;A,m3gfLk8`!fFr-ik_0U#d{cTv08BQ0_)AEg');
define('LOGGED_IN_SALT',   't.%WNTKBfuko`a96a%%Xtyhm-,3 0ph1EGh/rh0!(NY^&1Wqew.h0Wu.-AXH-Y/!');
define('NONCE_SALT',       '(P$BdjTI0]pb+CC7{ !^WGvU|=k7?,.alfISge+$EABx|k2z1?Wb3nQbHM99/ $4');


/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


if ( isset( $_SERVER['HTTP_HOST'] ) && preg_match('/^(gearcoopblogdev.)\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}(.xip.io)\z/', $_SERVER['HTTP_HOST'] ) ) {
define( 'WP_HOME', 'http://' . $_SERVER['HTTP_HOST'] );
define( 'WP_SITEURL', 'http://' . $_SERVER['HTTP_HOST'] );
}


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

/** Increase Memory */
define('WP_MEMORY_LIMIT', '3000M');
