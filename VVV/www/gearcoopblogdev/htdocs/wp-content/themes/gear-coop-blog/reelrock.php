<?php
/*
 Template Name: Reelrock
 */
?>
<?php
get_header(); ?>

      <!-- <div class="container-fluid container-image-single" wp-loop>
  <div class="background-featured-image" wp-featured-bck wp-featured-bck-size="full"></div>
</div> -->
      <div class="container-fluid container-content container-content-single">
          <div class="row">
              <div id="load"></div>
              <div class="col-single-content col-md-12">
                  <div class="rr11-header">
                      <!-- <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/logo-white.png" /> -->
                      <h1 class="text-uppercase"><?php _e( 'Gear Coop', 'gearcoopblogtheme' ); ?></h1>
                      <h1 class="text-uppercase"><?php _e( '#getyourselfieoutdoors', 'gearcoopblogtheme' ); ?></h1>
                      <h1 class="text-uppercase"><?php _e( '#reelrock11', 'gearcoopblogtheme' ); ?></h1>
                  </div>
                  <?php if ( have_posts() ) : ?>
                      <?php while ( have_posts() ) : the_post(); ?>
                          <div class="col-post-content col-md-9">
                              <div id="contents" class="post-content">
                                  <?php the_content(); ?>
                              </div>
                          </div>
                      <?php endwhile; ?>
                  <?php else : ?>
                      <p><?php _e( 'Sorry, no posts matched your criteria.', 'gearcoopblogtheme' ); ?></p>
                  <?php endif; ?>
              </div>
          </div>
      </div>

<?php get_footer(); ?>
