
        </main>
        <footer class="navbar-fixed-bottom">
            <div class="container-footer">
                <div class="row">
                    <div class="col-md-12 col-footer text-center">
                        <a href="#0" class="cd-top text-center"><span style="color:#FFE;font-weight: bolder;">▲</span></a>
                        <p class="copy"><a href="https://www.gearcoop.com" class="h-link"><?php _e( 'Gear Coop', 'gearcoopblogtheme' ); ?></a> &copy; <?php echo date('Y') ?></p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Bootstrap core JavaScript
    ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <?php wp_footer(); ?>
    </body>
</html>
