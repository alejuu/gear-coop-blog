<?php
/*
Template Name: Single Post Template
Single Post Template: [Descriptive Template Name]
Description: This part is optional, but helpful for describing the Post Template
*/
?>
<?php
get_header(); ?>

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <div class="container-fluid container-image-single">
            <?php $image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' ); ?>
            <div class="background-featured-image" style="<?php if($image_attributes) echo 'background-image:url(\''.$image_attributes[0].'\')' ?>"></div>
        </div>
    <?php endwhile; ?>
<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>
<div class="container-content container-content-single container-fluid">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <div class="col-md-12 col-single-content">
                        <div class="col-post-content col-md-12">
                            <h2 class="post-title text-uppercase"><?php the_title(); ?></h2>
                            <p class="post-date"><?php echo get_the_time( get_option( 'date_format' ) ) ?></p>
                            <p class="post-author"> <?php _e( '| Words by', 'gearcoopblogtheme' ); ?> <a href="http://" class="post-author-link">
                                    <?php the_author_posts_link(); ?>
                                </a></p>
                            <p class="post-content"><?php the_content(); ?></p>
                            <p class="tags italic"><?php the_tags(); ?></p>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
