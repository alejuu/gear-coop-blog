module.exports = function(grunt) {
	require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);
	grunt.initConfig({
		'pkg': grunt.file.readJSON('package.json'),
		'cssmin': {
			'min': {
				'src': 'autoprefixer-style.css',
				'dest': 'compiled.min.css'
			}
		},
		'concat': {
			options: {
				separator: ';'
			},
			dist: {
				src: ['assets/js/scripts/*.js'],
				dest: 'assets/js/internal.js'
			}
		},
    'uglify': {
      my_target: {
        files: {
          'assets/js/internal.min.js': ['assets/js/internal.js']
        }
      }
    },
		'autoprefixer': {
      dist: {
        files: {
          'autoprefixer-style.css': 'style.css'
        }
      }
    },
    'watch': {
      styles: {
        files: ['style.css'],
        tasks: ['autoprefixer']
      }
    }
	});
	grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-watch');
	// Default task(s).
	grunt.registerTask( 'default' , [ 'autoprefixer','cssmin','concat','uglify' ] );
  grunt.registerTask( 'css' , [ 'cssmin' ] );
  grunt.registerTask( 'js' , ['concat','uglify' ] );
	grunt.registerTask( 'prefix' , [ 'autoprefixer' ] );
};
