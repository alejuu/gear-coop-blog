<div class="comments">
  <h3 class="title-comments"><?php printf( _nx( 'One thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', get_comments_number(), 'comments title', 'gearcoopblogtheme' ), number_format_i18n( get_comments_number() ), get_the_title() ); ?></h3>
  <?php wp_list_comments(); ?>
  <?php comment_form(); ?>
</div>