<?php
/*
 Template Name: Van Life 2
 */
?>
<?php
get_header(); ?>
      <!--Hero-->
      <div class="container-fluid ltvl-hero">
        <img class="ltvl-logo" src="../wp-content/themes/gear-coop-blog/assets/images/LTVL_logo.png" />
        <p>We are looking for our next van life ambassador who is eager to share their outdoor journeys with us and our community to strengthen the storytelling that you’ve come to expect from Gear Coop</p>
        <h6 class="text-uppercase"><b>Last Call January 31, 2017</b></h6>
        <a href="#apply"><button type="button" class="btn btn-ltvl">Apply Now</button></a>
      </div>
      <!--Hero-->
      <!--container 1-->
        <div class="container container-content container-content-single">
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div class="col-md-12 col-single-content">
                  <div class="col-post-content col-md-12">
                      <div id="apply" class="post-content">
                          <div class="col-md-12 ltvl-steps">
                              <div class="col-md-3 col-sm-6 ltvl-step text-center text-uppercase">
                                  <h4><b>Step 1</b></h4>
                                  <img src="" />
                                  <h5>Take a Photo & Write Your Story</h5>
                              </div>
                              <div class="col-md-3 col-sm-6 ltvl-step text-center text-uppercase">
                                  <h4><b>Step 2</b></h4>
                                  <img src="" />
                                  <h5>Apply To Become an Ambassador</h5>
                              </div>
                              <div class="col-md-3 col-sm-6 ltvl-step text-center text-uppercase">
                                  <h4><b>Step 3</b></h4>
                                  <img src="" />
                                  <h5>Follow along on our Social Channels</h5>
                              </div>
                              <div class="col-md-3 col-sm-6 ltvl-step text-center text-uppercase">
                                  <h4><b>Step 4</b></h4>
                                  <img src="" />
                                  <h5>Crack open a drink & relax</h5>
                              </div>
                          </div>


                      </div>
                  </div>
              </div>
            </div>
          </div>
        </div>

      <!--container 2-->
      <div class="container-fluid ltvl-get">
        <div class="container">
          <div class="row">
            <div class="col-md-12 ltvl-info text-center">
                <h2 class="text-uppercase"><b>What You'll Get</b></h2>
                <!-- <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p> -->
                <div class="col-md-12 ltvl-sponsors">
                    <div class="col-md-15 col-sm-15 pad-10">
                      <div class="sponsor">
                        <img width="155" src="../wp-content/themes/gear-coop-blog/assets/images/tnf.png" />
                      </div>
                        <p>Tent</p>
                        <p>Van repairs</p>
                    </div>
                    <div class="col-md-15 col-sm-15 pad-10">
                      <div class="sponsor">
                        <img width="120" src="../wp-content/themes/gear-coop-blog/assets/images/arcteryx.png" />
                      </div>
                        <p>2 tickets to Climbing Academy - gas money to get there and hotel accommodations</p>
                        <p>Summer outfit - Atom SL, Harness, Acrux SL Shoes, Arc’teryx Word Shirt, Shorts, Belt, Cap, other (Coffee Mug, Stickers)</p>
                        <p>Winter Outfit - Available in September</p>
                    </div>
                    <div class="col-md-15 col-sm-15 pad-10">
                      <div class="sponsor">
                        <img width="120" src="../wp-content/themes/gear-coop-blog/assets/images/lasportiva.png" />
                      </div>
                        <p>One pair of climbing shoes</p>
                        <p>One pair of approach shoes</p>
                        <p>One pair of hiking shoes</p>
                        <p>One chalk bag</p>
                        <p>One rope bag</p>
                    </div>
                    <div class="col-md-15 col-sm-15 pad-10">
                      <div class="sponsor">
                        <img width="80" src="../wp-content/themes/gear-coop-blog/assets/images/deuter.png" />
                      </div>
                        <p>Packs</p>
                    </div>
                    <div class="col-md-15 col-sm-15 pad-10">
                      <div class="sponsor">
                        <img width="155" src="../wp-content/themes/gear-coop-blog/assets/images/olukai.png" />
                      </div>
                      <p>2-4 pairs of shoes, seeded seasonally</p>
                      <p>OluKai trip (contingent on winner's personality, interests, abilities)</p>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
      <!--container 3-->
      <div class="container-fluid ltvl-return">
        <div class="container">
          <div class="row">
            <div class="col-md-12 ltvl-info text-center">
              <h2 class="text-uppercase"><b>In return</b></h2>
              <ul class="return">
                <li>
                  Blog posts on Gear Coop
                </li>
                <li>
                  Gear reviews
                </li>
                <li>
                  Product shots
                </li>
                <li>
                  Featured on our homepage
                </li>
                <li>
                  Featured on Instagram, Facebook, and Twitter
                </li>
                <li>
                  Featured on new ambassador’s social channels
                </li>
                <li>
                  Email campaigns
                </li>
                <li>
                  PR
                </li>
              </ul>
              <h4 class="text-uppercase">Who will our new ambassador be?</h4>
              <p>We are looking for an ambassador with a strong following on social media.  They will have strong writing and photography skills.</p>
            </div>
          </div>
        </div>
      </div>
      <!--container 4-->
      <div class="container-fluid ltvl-apply">
        <div class="row">
          <div class="col-md-12 text-center col-apply">
              <button type="button" class="btn btn-apply" data-toggle="modal" data-target="#ltvlModal">Apply Now</button>
          </div>
          <div id="ltvlModal" class="modal fade" role="dialog">
              <div class="modal-dialog">
                <!-- Modal content-->
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <!-- <h4 class="modal-title">Modal Header</h4> -->
                      </div>
                      <div class="modal-body">
                          <?php the_content(); ?>
                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
                      </div>
                  </div>
              </div>
          </div>
        </div>
      </div>
      <!--container content-->
      <div class="container container-content container-content-single">
          <div class="row">
              <div class="col-md-12 col-sm-12">
                  <?php
                      $vanlife_args = array(
                        'post_type' => 'vanlife'
                      )
                  ?>
                  <?php $vanlife = new WP_Query( $vanlife_args ); ?>
                  <?php if ( $vanlife->have_posts() ) : ?>
                      <div class="col-md-12 col-posts">
                          <?php while ( $vanlife->have_posts() ) : $vanlife->the_post(); ?>
                              <article class="post-article">
                                  <div class="col-md-4 col-post col-sm-6">
                                      <div class="eq-height">
                                          <!-- <a href="<?php echo esc_url( wp_get_shortlink()); ?>"> -->
                                              <?php $image_attributes = (is_singular() || in_the_loop()) ? wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'thumbnail') : null; ?>
                                              <div id="ltvl-entry" class="thumb-image text-center" style="<?php if($image_attributes) echo 'background-image:url(\''.$image_attributes[0].'\')' ?>">
                                                <!-- <a class="full-link">Check it Out</a> -->
                                                <h3 class="text-uppercase ltvl-entry-title ltvl-show"><?php the_field('first_name'); ?></h3>
                                                <h3 class="text-uppercase ltvl-entry-title ltvl-show"><?php the_field('last_name'); ?></h3>
                                                <div class="ltvl-entry-content ltvl-show">
                                                    <?php the_content(); ?>
                                                </div>
                                                <div class="ltvl-sm ltvl-show">
                                                    <a class="link-ltvl" href="<?php the_field('ins_link'); ?>" target="blank">
                                                        <img src="../wp-content/themes/gear-coop-blog/assets/images/i-black.png" alt="Instagram" width="32" class="ltvl-sm-icon"/>
                                                    </a>
                                                    <a class="link-ltvl" href="<?php the_field('fb_link'); ?>" target="blank">
                                                        <img src="../wp-content/themes/gear-coop-blog/assets/images/fb-black.png" alt="Facebook" width="32" class="ltvl-sm-icon"/>
                                                    </a>
                                                    <a class="link-ltvl" href="<?php the_field('tw_link'); ?>" target="blank">
                                                        <img src="../wp-content/themes/gear-coop-blog/assets/images/tw-black.png" alt="Twitter" width="32" class="ltvl-sm-icon"/>
                                                    </a>
                                                    <a class="link-ltvl" href="<?php the_field('www_link'); ?>" target="blank">
                                                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAaNJREFUeNp000kohGEcx/GZwQxJHORkudgSWbJkyZKIlIssR+ukOLhw4SQ3ipNwcScuLnIQkUmWCBdcJlG2XIgw+D76jd4Mb32aZ973+f+f7f/YbYFPBsqRhwK928YO1nBk7RxkaTswgAZEohA3eEQSnlGptgefJsiuYCdGEIJR9GnUUH1/0Wwm0I83DOHVoQ5udClzPlxYUGKn2i5986iv27+ENLRiGNkYxDWCUYpYhCELPbjCJOpxYhK04w4zuEQ6VpGihNGa8jHCMYYtpCLRjJKAWS3FbNAepvS/W7/+/zHqc4pltJlN3MAmblGkUVa0y2UKXNeGV+NJMzDJih2/asDuP55/nk/LyX33C1ZhLKpQzpCJccvxWZdgZneIJRVapEngRY0SnKNTa48zU1SgaV8gV0dqU4zXnMIDWkxRoBZ1GnlDFXmvNVdoT3yIQBWm/WvrVaImNKsazdMhNr1rVp8HxfzchQNEIR7zKNFmmdJ+VzElYw6N2FVCn/3XZerXbfRpvV59S1BQkDbdBH9YL9Nf19lcnhy929flCrjOXwIMANRqYeEhTz1jAAAAAElFTkSuQmCC" alt="Web Site" width="32" class="ltvl-sm-icon"/>
                                                    </a>
                                                </div>
                                                <!-- <a href="<?php echo esc_url( wp_get_shortlink()); ?>" class="post-read-more-link"><p class="post-read-more"><?php _e( 'Read More', 'gearcoopblogtheme' ); ?></p></a> -->
                                              </div>
                                          <!-- </a> -->
                                      </div>
                                  </div>
                              </article>
                          <?php endwhile; ?>
                          <?php wp_reset_postdata(); ?>
                          <?php echo do_shortcode('[ajax_load_more offset="3" category__not_in="9, 3, 27, 4, 43" transition_container="false"]');?>
                          <div class="col-md-12 col-pagination text-left col-sm-12 col-xs-12">
                              <?php wp_bootstrap_pagination( array() ); ?>
                          </div>
                      </div>
                  <?php else : ?>
                      <p><?php _e( 'Sorry, no posts matched your criteria.', 'gearcoopblogtheme' ); ?></p>
                  <?php endif; ?>
              </div>
          </div>
      </div>

<?php get_footer(); ?>
