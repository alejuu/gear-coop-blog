<?php
get_header(); ?>

            <div class="container container-content"> 
                <div class="row"> 
                    <div class="col-md-9 col-sm-9"> 
                        <?php if ( have_posts() ) : ?>
                            <div class="col-md-12 col-posts"> 
                                <h2 class="title text-uppercase"><?php single_tag_title(); ?></h2> 
                                <?php while ( have_posts() ) : the_post(); ?>
                                    <article class="post-article"> 
                                        <div class="col-md-4 col-post col-sm-6"> 
                                            <div class="eq-height"> 
                                                <a href="<?php echo esc_url( wp_get_shortlink()); ?>"> 
                                                    <?php $image_attributes = (is_singular() || in_the_loop()) ? wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'medium' ) : null; ?>
                                                    <div class="thumb-image" style="<?php if($image_attributes) echo 'background-image:url(\''.$image_attributes[0].'\')' ?>"></div>                                                     
                                                </a>                                                 
                                                <a href="<?php echo esc_url( wp_get_shortlink()); ?>"><h3 class="post-title h-link text-uppercase"><?php the_title(); ?></h3></a> 
                                                <p class="post-date"><?php echo get_the_time( get_option( 'date_format' ) ) ?></p> 
                                                <p class="post-author"> <?php _e( '| Words by', 'gearcoopblogtheme' ); ?> <a href="http://" class="post-author-link"><?php the_author_posts_link(); ?></a></p> 
                                                <p class="post-excerpt"><?php the_excerpt( ); ?></p> 
                                                <a href="<?php echo esc_url( wp_get_shortlink()); ?>" class="post-read-more-link"><p class="post-read-more"><?php _e( 'Read More', 'gearcoopblogtheme' ); ?></p></a> 
                                            </div>                                             
                                        </div>                                         
                                    </article>
                                <?php endwhile; ?> 
                                <?php
$tag = get_query_var('tag');
echo do_shortcode('[ajax_load_more tag="'.$tag.'" offset="3"]');
?> 
                                <div class="col-md-12 col-pagination text-left col-sm-12 col-xs-12"> 
                                    <?php wp_bootstrap_pagination( array() ); ?> 
                                </div>                                 
                            </div>
                        <?php else : ?>
                            <p><?php _e( 'Sorry, no posts matched your criteria.', 'gearcoopblogtheme' ); ?></p>
                        <?php endif; ?> 
                    </div>                     
                    <div class="col-md-3 col-sm-3 col-sidebar col-xs-12"> 
                        <h5 class="h-text text-uppercase"><?php _e( 'Gear Coop Blog', 'gearcoopblogtheme' ); ?></h5> 
                        <p class="italic grey"><?php _e( 'find out more about us at', 'gearcoopblogtheme' ); ?> <a href="http://gearcoop.com/"><?php _e( 'gearcoop.com', 'gearcoopblogtheme' ); ?></a></p> 
                        <div class="social-media-links"> 
                            <a href="https://www.facebook.com/gearcoop" target="blank"> 
                                <img class="social-icons" src="http://blog.gearcoop.com/wp-content/uploads/2016/03/fb.png" /> 
                            </a>                             
                            <a href="https://twitter.com/gearcoop" target="blank"> 
                                <img class="social-icons" src="http://blog.gearcoop.com/wp-content/uploads/2016/03/tw.png" /> 
                            </a>                             
                            <a href="http://instagram.com/gearcoop" target="blank"> 
                                <img class="social-icons" src="http://blog.gearcoop.com/wp-content/uploads/2016/03/ig.png" /> 
                            </a>                             
                            <a href="https://plus.google.com/+Gearcoop" target="blank"> 
                                <img class="social-icons" src="http://blog.gearcoop.com/wp-content/uploads/2016/03/gp.png" /> 
                            </a>                             
                            <a href="https://www.youtube.com/c/Gearcoop" target="blank"> 
                                <img class="social-icons" src="http://blog.gearcoop.com/wp-content/uploads/2016/03/yt.png" /> 
                            </a>                             
                            <a href="http://www.pinterest.com/gearcoop" target="blank"> 
                                <img class="social-icons" src="http://blog.gearcoop.com/wp-content/uploads/2016/03/pt.png" /> 
                            </a>                             
                        </div>                         
                        <?php if ( is_active_sidebar( 'right_sidebar' ) ) : ?>
                            <div class="col-md-12 col-sm-12 col-widget">
                                <?php dynamic_sidebar( 'right_sidebar' ); ?>
                            </div>
                        <?php endif; ?> 
                    </div>                     
                </div>                 
            </div>                         

<?php get_footer(); ?>