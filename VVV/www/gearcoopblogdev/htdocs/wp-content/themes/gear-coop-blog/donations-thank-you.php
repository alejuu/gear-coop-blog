<?php
/*
 Template Name: Donations - Thank You
 */
?>
<?php
get_header(); ?>
<nav class="navbar navbar-default navbar-fixed-top clear-nav">
  <div class="container">
    <ul class="nav navbar-nav">
       <li><a href="http://www.gearcoop.com/">Store</a></li>
       <li><a href="http://www.blog.gearcoop.com/">Blog</a></li>
     </ul>
  </div>
</nav>
<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <div class="container-fluid container-image-single">
            <?php $image_attributes = (is_singular() || in_the_loop()) ? wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' ) : null; ?>
            <div class="background-featured-image full-center-height" style="<?php if($image_attributes) echo 'background-image:url(\''.$image_attributes[0].'\')' ?>">
                <h2 class="post-title text-uppercase"><?php the_title(); ?></h2>
            </div>
        </div>
    <?php endwhile; ?>
<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.', 'gearcoopblogtheme' ); ?></p>
<?php endif; ?>
<div class="container-content container-content-single container">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <div class="col-md-12 col-single-content">
                        <div class="col-post-content col-md-12">
                            <div class="post-content">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.', 'gearcoopblogtheme' ); ?></p>
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="container-fluid container-featured-donation">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-uppercase"><?php _e( 'Featured Blogs', 'gearcoopblogtheme' ); ?></h3>
            </div>
            <?php
                $feautured_args = array(
                  'category_name' => 'featured-story'
                )
            ?>
            <?php $feautured = new WP_Query( $feautured_args ); ?>
            <?php if ( $feautured->have_posts() ) : ?>
                <div class="col-md-12 col-posts">
                    <?php while ( $feautured->have_posts() ) : $feautured->the_post(); ?>
                        <article class="post-article">
                            <div class="col-md-4 col-post col-sm-6">
                                <div class="eq-height">
                                    <a href="<?php echo esc_url( wp_get_shortlink()); ?>">
                                        <?php $image_attributes = (is_singular() || in_the_loop()) ? wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'medium' ) : null; ?>
                                        <div class="thumb-image" style="<?php if($image_attributes) echo 'background-image:url(\''.$image_attributes[0].'\')' ?>"></div>
                                    </a>
                                    <a href="<?php echo esc_url( wp_get_shortlink()); ?>"><h3 class="post-title h-link text-uppercase"><?php the_title(); ?></h3></a>
                                    <p class="post-date"><?php echo get_the_time( get_option( 'date_format' ) ) ?></p>
                                    <p class="post-author"> <?php _e( '| Words by', 'gearcoopblogtheme' ); ?> <a href="http://" class="post-author-link"><?php the_author_posts_link(); ?></a></p>
                                    <p class="post-excerpt"><?php the_excerpt( ); ?></p>
                                    <a href="<?php echo esc_url( wp_get_shortlink()); ?>" class="post-read-more-link"><p class="post-read-more"><?php _e( 'Read More', 'gearcoopblogtheme' ); ?></p></a>
                                </div>
                            </div>
                        </article>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                </div>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.', 'gearcoopblogtheme' ); ?></p>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
