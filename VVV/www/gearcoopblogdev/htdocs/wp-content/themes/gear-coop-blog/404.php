<?php
get_header(); ?>

<div class="container-fluid container-404"> 
    <div class="container container-content"> 
        <div class="row"> 
            <div class="col-md-9 col-sm-9"> 
                <h1 class="h1-404"><?php _e( 'Oops!!!', 'gearcoopblogtheme' ); ?></h1> 
                <h3 class="h3-404"><?php _e( '404', 'gearcoopblogtheme' ); ?></h3> 
                <p class="p-404"><?php _e( 'Page Not Found', 'gearcoopblogtheme' ); ?></p> 
                <a href="<?php echo esc_url( get_home_url() ); ?>"> 
                    <button type="button" class="btn btn-404">
                        <?php _e( 'Go To Home', 'gearcoopblogtheme' ); ?>
                    </button>                                 
                </a>                             
            </div>                         
            <div class="col-md-3 col-sm-3 col-sidebar col-xs-12"> 
                <h5 class="h-text text-uppercase"><?php _e( 'Gear Coop Blog', 'gearcoopblogtheme' ); ?></h5> 
                <p class="italic grey"><?php _e( 'find out more about us at', 'gearcoopblogtheme' ); ?> <a href="http://gearcoop.com/">
                        <?php _e( 'gearcoop.com', 'gearcoopblogtheme' ); ?>
                    </a></p> 
                <div class="social-media-links"> 
                    <a href="https://www.facebook.com/gearcoop" target="blank">
                        <img class="social-icons" src="http://blog.gearcoop.com/wp-content/uploads/2016/03/fb.png" />
                    </a>                                 
                    <a href="https://twitter.com/gearcoop" target="blank">
                        <img class="social-icons" src="http://blog.gearcoop.com/wp-content/uploads/2016/03/tw.png" />
                    </a>                                 
                    <a href="http://instagram.com/gearcoop" target="blank">
                        <img class="social-icons" src="http://blog.gearcoop.com/wp-content/uploads/2016/03/ig.png" />
                    </a>                                 
                    <a href="https://plus.google.com/+Gearcoop" target="blank"> 
                        <img class="social-icons" src="http://blog.gearcoop.com/wp-content/uploads/2016/03/gp.png" />
                    </a>                                 
                    <a href="https://www.youtube.com/c/Gearcoop" target="blank">
                        <img class="social-icons" src="http://blog.gearcoop.com/wp-content/uploads/2016/03/yt.png" />
                    </a>                                 
                    <a href="http://www.pinterest.com/gearcoop" target="blank">
                        <img class="social-icons" src="http://blog.gearcoop.com/wp-content/uploads/2016/03/pt.png" />
                    </a>                                 
                </div>                             
                <?php if ( is_active_sidebar( 'right_sidebar' ) ) : ?>
                    <div class="col-md-12 col-sm-12 col-widget">
                        <?php dynamic_sidebar( 'right_sidebar' ); ?>
                    </div>
                <?php endif; ?> 
            </div>                         
        </div>                     
    </div>                 
</div>                         

<?php get_footer(); ?>