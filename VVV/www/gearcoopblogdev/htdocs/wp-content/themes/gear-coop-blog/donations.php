<?php
/*
 Template Name: Donations
 */
?>
<?php
get_header(); ?>
<nav class="navbar navbar-default navbar-fixed-top clear-nav">
  <div class="container">
    <ul class="nav navbar-nav">
       <li><a href="http://www.gearcoop.com/">Store</a></li>
       <li><a href="http://www.blog.gearcoop.com/">Blog</a></li>
     </ul>
  </div>
</nav>
<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <div>
            <div class="container-fluid container-image-single">
                <?php $image_attributes = (is_singular() || in_the_loop()) ? wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' ) : null; ?>
                <div class="background-featured-image full-center-height" style="<?php if($image_attributes) echo 'background-image:url(\''.$image_attributes[0].'\')' ?>">
                    <h2 class="post-title text-uppercase"><?php the_title(); ?></h2>
                    <p><?php _e( 'Support the places we play in', 'gearcoopblogtheme' ); ?></p>
                    <a class="text-uppercase donate-down-btn" href="#donate-btn"><?php _e( 'Donate', 'gearcoopblogtheme' ); ?></a>
                    <a class="down-btn" href="#donate-btn"><span class="bottom"></span></a>
                </div>
            </div>
            <div class="container-content container-content-single container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="col-md-12 col-single-content">
                            <div class="col-post-content col-md-12">
                                <div class="post-content">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid container-image-single">
                <div class="background-featured-image feat-bottom">
                    <h3 class="text-uppercase"><?php _e( 'Support the places we play in', 'gearcoopblogtheme' ); ?></h3>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.', 'gearcoopblogtheme' ); ?></p>
<?php endif; ?>

<?php get_footer(); ?>
