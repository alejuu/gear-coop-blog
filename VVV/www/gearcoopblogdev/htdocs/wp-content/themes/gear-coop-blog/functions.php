<?php
if ( ! function_exists( 'gearcoopblogtheme_setup' ) ) :

function gearcoopblogtheme_setup() {

    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     */
    load_theme_textdomain( 'gearcoopblogtheme', get_template_directory() . '/languages' );

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
     * Let WordPress manage the document title.
     */
    add_theme_support( 'title-tag' );

    /*
     * Enable support for Post Thumbnails on posts and pages.
     */
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 825, 510, true );

    // Add menus.
    register_nav_menus( array(
        'primary' => __( 'Primary Menu',      'gearcoopblogtheme' ),
        'social'  => __( 'Social Links Menu', 'gearcoopblogtheme' ),
    ) );

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
        'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
    ) );

    /*
     * Enable support for Post Formats.
     */
    add_theme_support( 'post-formats', array(
        'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
    ) );
}
endif; // gearcoopblogtheme_setup

add_action( 'after_setup_theme', 'gearcoopblogtheme_setup' );


if ( ! function_exists( 'gearcoopblogtheme_init' ) ) :

function gearcoopblogtheme_init() {


    // Use categories and tags with attachments
    register_taxonomy_for_object_type( 'category', 'attachment' );
    register_taxonomy_for_object_type( 'post_tag', 'attachment' );

    /*
     * Register custom post types. You can also move this code to a plugin.
     */
    /* Pinegrow generated Custom Post Types Begin */

    /* Pinegrow generated Custom Post Types End */

    /*
     * Register custom taxonomies. You can also move this code to a plugin.
     */
    /* Pinegrow generated Taxonomies Begin */

    /* Pinegrow generated Taxonomies End */

}
endif; // gearcoopblogtheme_setup

add_action( 'init', 'gearcoopblogtheme_init' );


if ( ! function_exists( 'gearcoopblogtheme_widgets_init' ) ) :

function gearcoopblogtheme_widgets_init() {

    /*
     * Register widget areas.
     */
    /* Pinegrow generated Register Sidebars Begin */

    register_sidebar( array(
    'name' => __( 'Right Sidebar', 'gearcoopblogtheme' ),
    'id' => 'right_sidebar',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => '</li>',
    'before_title' => '<h3 class="widgettitle">',
    'after_title' => '</h3>'
  ) );

    /* Pinegrow generated Register Sidebars End */
}
add_action( 'widgets_init', 'gearcoopblogtheme_widgets_init' );
endif;// gearcoopblogtheme_widgets_init



if ( ! function_exists( 'gearcoopblogtheme_customize_register' ) ) :

function gearcoopblogtheme_customize_register( $wp_customize ) {
    // Do stuff with $wp_customize, the WP_Customize_Manager object.

    /* Pinegrow generated Customizer Controls Begin */

    $wp_customize->add_section( 'jumbo_section', array(
    'title' => __( 'Jumbotron Section', 'gearcoopblogtheme' )
  ));

    $wp_customize->add_setting( 'jumbo_bkg', array(
    'type' => 'theme_mod'
  ));

    $wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'jumbo_bkg', array(
    'label' => __( 'Jumbotron Background', 'gearcoopblogtheme' ),
    'type' => 'media',
    'mime_type' => 'image',
    'section' => 'jumbo_section'
  ) ) );

    $wp_customize->add_setting( 'jumbo_logo', array(
    'type' => 'theme_mod'
  ));

    $wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'jumbo_logo', array(
    'label' => __( 'Jumbotron Logo', 'gearcoopblogtheme' ),
    'type' => 'media',
    'mime_type' => 'image',
    'section' => 'jumbo_section'
  ) ) );

    $wp_customize->add_setting( 'jumbo_title', array(
    'type' => 'theme_mod',
    'default' => 'Hello, world!'
  ));

    $wp_customize->add_control( 'jumbo_title', array(
    'label' => __( 'Jumbotron Title', 'gearcoopblogtheme' ),
    'type' => 'text',
    'section' => 'jumbo_section'
  ));

    $wp_customize->add_setting( 'jumbo_desc', array(
    'type' => 'theme_mod',
    'default' => 'This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.'
  ));

    $wp_customize->add_control( 'jumbo_desc', array(
    'label' => __( 'Jumbo Description', 'gearcoopblogtheme' ),
    'type' => 'text',
    'section' => 'jumbo_section'
  ));

    /* Pinegrow generated Customizer Controls End */

}
add_action( 'customize_register', 'gearcoopblogtheme_customize_register' );
endif;// gearcoopblogtheme_customize_register


if ( ! function_exists( 'gearcoopblogtheme_enqueue_scripts' ) ) :
    function gearcoopblogtheme_enqueue_scripts() {

        /* Pinegrow generated Enqueue Scripts Begin */
    wp_deregister_script( 'jquery' );
    wp_enqueue_script( 'jquery', get_template_directory_uri() . '/assets/js/jquery.min.js', false, null, true);

    wp_deregister_script( 'internal' );
    wp_enqueue_script( 'internal', get_template_directory_uri() . '/assets/js/internal.min.js', false, null, true);

    wp_deregister_script( 'bootstrap' );
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/bootstrap/js/bootstrap.min.js', false, null, true);

    /* Pinegrow generated Enqueue Scripts End */

        /* Pinegrow generated Enqueue Styles Begin */

    wp_deregister_style( 'bootstrap' );
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/bootstrap/css/bootstrap.min.css', false, null, 'all');

    wp_deregister_style( 'compiled' );
    wp_enqueue_style( 'compiled', get_template_directory_uri() . '/compiled.min.css', false, null, 'all');

    wp_deregister_style( 'style-1' );
    wp_enqueue_style( 'style-1', 'https://fonts.googleapis.com/css?family=Source+Sans+Pro', false, null, 'all');

    /* Pinegrow generated Enqueue Styles End */

    }
    add_action( 'wp_enqueue_scripts', 'gearcoopblogtheme_enqueue_scripts' );
endif;

/*
 * Resource files included by Pinegrow.
 */
/* Pinegrow generated Include Resources Begin */
require_once "inc/bootstrap/wp_bootstrap_navwalker.php";
require_once "inc/bootstrap/wp_bootstrap_pagination.php";

    /* Pinegrow generated Include Resources End */

/**
* Filter the excerpt length to 20 characters.
*
* @param int $length Excerpt length.
* @return int (Maybe) modified excerpt length.
*/
function wpdocs_custom_excerpt_length( $length ) {
  return 25;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );
function new_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

/* Add class to link */
function add_menuclass($ulclass) {
   return preg_replace('/<a /', '<a class="menu-link"', $ulclass);
}
add_filter('wp_nav_menu','add_menuclass');

/* Author Social Media Icons */
function add_to_author_profile( $contactmethods ) {
  $contactmethods['rss_url'] = 'RSS URL';
  $contactmethods['google_profile'] = 'Google Profile URL';
  $contactmethods['twitter_profile'] = 'Twitter Profile URL';
  $contactmethods['facebook_profile'] = 'Facebook Profile URL';
  $contactmethods['instagram_profile'] = 'Instagram Profile URL';
  $contactmethods['website_profile'] = 'Website Profile URL';
  return $contactmethods;
}
add_filter( 'user_contactmethods', 'add_to_author_profile', 10, 1);
/* Give */
function my_custom_give_email_content( $email_body, $payment_id, $payment_data ) {
	$amount = give_get_payment_amount($payment_id);
	$form_id = give_get_payment_form_id($payment_id);
	//Check if this payment's donation amount and form ID matches the donation form we want custom email body copy

	if ( $amount < 20 && $form_id == '5341'  ) {
		//Here you can output custom content or pull from custom post meta using get_post_meta, or use an ACF field, WordPress functions, etc.
		//You can also use Give's email tags like {price}, {fullname}, etc.

		// $email_body = '<p>Hi {name}, </p>';
		// $email_body .= '<p> your donation to {form_title} was for {price} and that\'s great!</p>';
    $email_body = '<table style="width:100%;max-width:600px;margin:0 auto;" align="center">
  <tr>
    <th>
      <img width="100%" src="https://blog.gearcoop.com/wp-content/uploads/2017/04/npw_email_header.jpg">
    </th>
  </tr>
  <tr>
    <th align="left">
      <h3>Hi {name},</h3>
      <p style="font-weight:normal">Thank you for supporting our National Parks! Your National Park Week Sticker will be on its way soon! Please allow 3 weeks for it to arrive.</p>
      <p>Thank your four your support!</p>
      <p>
      <strong>Donor:</strong> {fullname}
<strong>Donation:</strong> {donation}
<strong>Donation Date:</strong> {date}
<strong>Amount:</strong> {amount}
<strong>Payment Method:</strong> {payment_method}
<strong>Payment ID:</strong> {payment_id}
<strong>Receipt ID:</strong> {receipt_id}

{receipt_link}



Sincerely,
{sitename}
      </p>
      <p>
      Cheer, Gear Coop
      </p>
    </th>
  </tr>
</table>';

		//Make sure we return the new $email_body;
		return $email_body;

	} elseif ( $amount > 19 && $form_id == '5341'  ) {
		//Here you can output custom content or pull from custom post meta using get_post_meta, or use an ACF field, WordPress functions, etc.
		//You can also use Give's email tags like {price}, {fullname}, etc.

		$email_body = '<table style="width:100%;max-width:600px;margin:0 auto;" align="center">
  <tr>
    <th>
      <img width="100%" src="https://blog.gearcoop.com/wp-content/uploads/2017/04/npw_email_header.jpg">
    </th>
  </tr>
  <tr>
    <th align="left">
      <h3>Hi {name},</h3>
      <p style="font-weight:normal">Thank you for supporting our National Parks! Your National Park Week Sticker will be on its way soon! Please allow 3 weeks for it to arrive.</p>
      <p style="font-weight:normal">Here is a $10 discount code for GearCoop.com Use your code at checkout:</p>
      <h4 style="text-transform:uppercase;font-size:24px" align="center">Use Code: #########</h4>
      <p>Thank your four your support!</p>
      <p>
      <strong>Donor:</strong> {fullname}
<strong>Donation:</strong> {donation}
<strong>Donation Date:</strong> {date}
<strong>Amount:</strong> {amount}
<strong>Payment Method:</strong> {payment_method}
<strong>Payment ID:</strong> {payment_id}
<strong>Receipt ID:</strong> {receipt_id}

{receipt_link}



Sincerely,
{sitename}
      </p>
      <p>
      Cheer, Gear Coop
      </p>
    </th>
  </tr>
</table>';

		//Make sure we return the new $email_body;
		return $email_body;

			//Be sure to return default email body if we don't make a match
		} else {
			return $email_body;
	}
}

add_filter( 'give_donation_receipt', 'my_custom_give_email_content', 10, 3 );
function my_custom_give_email_headings( $message ) {
	switch ( $message ) {
		case 'New Donation!':
			$message = 'Boom! New Donation';
			break;
		case 'Donation Receipt':
			$message = '';
			break;
	}
	return $message;
}
add_filter( 'give_email_heading', 'my_custom_give_email_headings', 10, 1 );
?>
