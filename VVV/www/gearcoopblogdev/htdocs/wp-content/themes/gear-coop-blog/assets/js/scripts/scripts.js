/*! Main */
jQuery(document).ready(function($) {
  //Top fixed Nav / show/hide #brand
  // Fixa navbar ao ultrapassa-lo
  var navbar = $('#navbar-main'),
  distance = navbar.offset().top,
  $window = $(window);
  $("#brand").hide(); //hide your div initially
  // browser window scroll (in pixels) after which the "back to top" link is shown
  var offset = 300,
  //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
  offset_opacity = 1200,
  //duration of the top scrolling animation (in ms)
  scroll_top_duration = 700,
  //grab the "back to top" link
  $back_to_top = $('.cd-top');
  $window.scroll(function() {
    if ($window.scrollTop() >= distance) {
      navbar.removeClass('navbar-fixed-top').addClass('navbar-fixed-top');
      $("body").css("padding-top", "0px");
      $("#brand").show("slow");
    }
    else {
      navbar.removeClass('navbar-fixed-top');
      $("body").css("padding-top", "0px");
      $("#brand").hide("slow");
    }
    //hide or show the "back to top" link
    ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if( $(this).scrollTop() > offset_opacity ) {
			$back_to_top.addClass('cd-fade-out');
		}
  });
  //smooth scroll to top
	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0 ,
		 	}, scroll_top_duration
		);
	});
});
//after ajax
//equal height
$(function() {
  $.fn.almComplete = function(alm){
    var bigbrother = -1;
    $('.col-post').each(function() {
      bigbrother = bigbrother > $('.col-post').height() ? bigbrother : $('.col-post').height();
    });
    $('.col-post').each(function() {
      $('.col-post').height(bigbrother);
    });
  };
});
