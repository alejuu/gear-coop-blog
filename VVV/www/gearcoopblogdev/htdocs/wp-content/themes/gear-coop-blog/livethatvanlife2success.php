<?php
/*
Template Name: Live That Van Life 2 Success
*/
?>
<?php
get_header(); ?>
<nav class="navbar navbar-default navbar-fixed-top ltvl-nav">
  <div class="container">
    <ul class="nav navbar-nav">
       <li><a href="http://www.gearcoop.com/">Store</a></li>
       <li><a href="http://www.blog.gearcoop.com/">Blog</a></li>
     </ul>
  </div>
</nav>
<!--Hero-->
<div class="container-fluid ltvl-hero">
  <img class="ltvl-logo" src="../wp-content/themes/gear-coop-blog/assets/images/LTVL_logo.png" />
  <h1 class="text-uppercase">Thanks for your application!</h1>
  <a href="/index.php?page_id=4637"><button type="button" class="btn btn-ltvl">Check Out the Sories</button></a>
</div>
<!--Hero-->
<?php get_footer(); ?>
