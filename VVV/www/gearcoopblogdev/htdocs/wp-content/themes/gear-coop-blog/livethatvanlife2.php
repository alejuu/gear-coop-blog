<?php
/*
Template Name: Live That Van Life 2
*/
?>
<?php
get_header(); ?>
<!--Hero-->
<nav class="navbar navbar-default navbar-fixed-top ltvl-nav">
  <div class="container">
    <ul class="nav navbar-nav">
       <li><a href="http://www.gearcoop.com/">Store</a></li>
       <li><a href="http://www.blog.gearcoop.com/">Blog</a></li>
     </ul>
  </div>
</nav>
<div class="container-fluid ltvl-hero">
  <!-- <h2 class="thanks"><?php echo do_shortcode('[ap-form-message]');?></h2> -->
  <img class="ltvl-logo" src="../wp-content/themes/gear-coop-blog/assets/images/LTVL_logo.png" />
  <p>Make the change from being a weekend warrior to a fully committed van life ambassador!</p>
  <h6 class="text-uppercase"><b>Last Call January 31, 2017</b></h6>
  <!-- <a href="#apply"><button type="button" class="btn btn-ltvl">Apply Now</button></a> -->
</div>
<!--Hero-->
<!--container 1-->
<div class="container container-content-single">
  <div class="row">
    <div class="col-md-12 ltvl-steps">
      <div class="col-md-3 col-sm-3 ltvl-step text-center text-uppercase">
        <h4><b>Step 1</b></h4>
        <h5>Take a Photo & Write Your Story</h5>
      </div>
      <div class="col-md-3 col-sm-3 ltvl-step text-center text-uppercase">
        <h4><b>Step 2</b></h4>
        <h5>Apply To Become an Ambassador</h5>
      </div>
      <div class="col-md-3 col-sm-3 ltvl-step text-center text-uppercase">
        <h4><b>Step 3</b></h4>
        <h5>Follow along on our Social Channels</h5>
      </div>
      <div class="col-md-3 col-sm-3 ltvl-step text-center text-uppercase">
        <h4><b>Step 4</b></h4>
        <h5>Crack open a drink & relax</h5>
      </div>
    </div>
  </div>
</div>
<!--container 2-->
<div class="container-fluid">
  <div class="container">
    <div class="row">
      <div class="col-md-12 ltvl-info text-center">
          <h2 class="text-uppercase"><b>Sponsors</b></h2>
          <div class="col-md-12 ltvl-sponsors">
              <div class="col-md-15 col-sm-15 pad-10">
                <div class="sponsor">
                  <img width="100" src="../wp-content/themes/gear-coop-blog/assets/images/tnf.png" />
                </div>
              </div>
              <div class="col-md-15 col-sm-15 col-xs-12 pad-10">
                <div class="sponsor">
                  <img width="90" src="../wp-content/themes/gear-coop-blog/assets/images/arcteryx.png" />
                </div>
              </div>
              <div class="col-md-15 col-sm-15 col-xs-12 pad-10">
                <div class="sponsor">
                  <img width="120" src="../wp-content/themes/gear-coop-blog/assets/images/lasportiva.png" />
                </div>
              </div>
              <div class="col-md-15 col-sm-15 col-xs-12 pad-10">
                <div class="sponsor">
                  <img width="70" src="../wp-content/themes/gear-coop-blog/assets/images/deuter.png" />
                </div>
              </div>
              <div class="col-md-15 col-sm-15 col-xs-12 pad-10">
                <div class="sponsor">
                  <img width="120" src="../wp-content/themes/gear-coop-blog/assets/images/olukai.png" />
                </div>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>
<!-- <div class="container-fluid">
<div class="container">
<div class="row">
</div>
</div>
</div> -->
<!--container 4-->
<div class="container-fluid">
  <div class="row">
    <div id="apply" class="col-md-12 text-center">
      <h2 class="text-uppercase"><b>Do you want to be our new ambassador?</b></h2>
      <p class="who-text">Do you know the ins and outs of living in a van?  Are you traveling the country psyched on your next adventure?  We are looking for our next Live That Van Life ambassador.  If you get outdoors nearly every weekend and you're already writing about it and posting amazing shots on your Instagram, you should apply now!  Our new ambassador is going to get top of the line gear from all of our Live That Van Life sponsors and even a few trips!</p>
      <button type="button" class="btn btn-apply" data-toggle="modal" data-target="#ltvlModal">Apply Now</button>
    </div>
    <div id="ltvlModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <!-- <h4 class="modal-title">Modal Header</h4> -->
          </div>
          <div class="modal-body">
            <?php the_content(); ?>
            <!--Mailchimp-->
            <!-- Begin MailChimp Signup Form -->

<div id="mc_embed_signup">
  <form action="//gearcoop.us6.list-manage.com/subscribe/post?u=9c1910151d2eeb138279baf98&amp;id=0e5e2fe783" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank">

    <div id="mc_embed_signup_scroll">


      <div class="mc-field-group" style="display:none">
	<label for="mce-FNAME">First Name </label>
	<input type="text" value="" name="FNAME" class="" id="mce-FNAME">
</div>
<div class="mc-field-group" style="display:none">
	<label for="mce-LNAME">Last Name </label>
	<input type="text" value="" name="LNAME" class="" id="mce-LNAME">
</div>

<div class="mc-field-group" style="display:none">
	<label for="mce-EMAIL">Email Address </label>
	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
</div>
<div class="mc-field-group input-group" style="display:none">
    <strong>Singup Location </strong>
    <ul>

<li><input type="checkbox" value="4096" name="group[12569][4096]" id="mce-group[12569]-12569-6" checked><label for="mce-group[12569]-12569-6">VanLife2</label></li>
</ul>
</div>
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none!important"></div>
		<div class="response" id="mce-success-response" style="display:none!important"></div>
	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_9c1910151d2eeb138279baf98_0e5e2fe783" tabindex="-1" value=""></div>
    <div class="clear"><input style="display:none" type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="ltvl-real-submit"></div>
    </div>
</form>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>

<!--End mc_embed_signup-->

            <!--Mailchimp-->


          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--container content-->
<div class="container container-content container-content-single">
  <div class="row">
    <div class="col-md-12 col-sm-12">
      <?php
      $vanlife_args = array(
        'post_type' => 'vanlife'
      )
      ?>
      <?php $vanlife = new WP_Query( $vanlife_args ); ?>
      <?php if ( $vanlife->have_posts() ) : ?>
        <div class="col-md-12 col-posts">
          <?php while ( $vanlife->have_posts() ) : $vanlife->the_post(); ?>
            <article class="post-article">
              <div class="col-md-4 col-post col-sm-6">
                <div class="eq-height">
                  <?php if ( has_post_thumbnail() ) { $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); } else { $feat_image = get_field('default_featured_image', 'option'); } ?>
                  <div id="ltvl-entry" class="thumb-image text-center" style="background-image: url(<?php echo $feat_image; ?>);">
                    <div class="ltvl-show">
                      <h3 class="text-uppercase ltvl-entry-title"><?php the_field('first_name'); ?></h3>
                      <h3 class="text-uppercase ltvl-entry-title"><?php the_field('last_name'); ?></h3>
                      <div class="ltvl-entry-content">
                        <?php the_excerpt(); ?>
                      </div>
                      <div class="ltvl-sm">
                        <a class="link-ltvl" href="<?php the_field('ins_link'); ?>" target="blank">
                          <img src="../wp-content/themes/gear-coop-blog/assets/images/i-black.png" alt="Instagram" width="32" class="ltvl-sm-icon"/>
                        </a>
                        <a class="link-ltvl" href="<?php the_field('fb_link'); ?>" target="blank">
                          <img src="../wp-content/themes/gear-coop-blog/assets/images/fb-black.png" alt="Facebook" width="32" class="ltvl-sm-icon"/>
                        </a>
                        <a class="link-ltvl" href="<?php the_field('tw_link'); ?>" target="blank">
                          <img src="../wp-content/themes/gear-coop-blog/assets/images/tw-black.png" alt="Twitter" width="32" class="ltvl-sm-icon"/>
                        </a>
                        <a class="link-ltvl" href="<?php the_field('www_link'); ?>" target="blank">
                          <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAaNJREFUeNp000kohGEcx/GZwQxJHORkudgSWbJkyZKIlIssR+ukOLhw4SQ3ipNwcScuLnIQkUmWCBdcJlG2XIgw+D76jd4Mb32aZ973+f+f7f/YbYFPBsqRhwK928YO1nBk7RxkaTswgAZEohA3eEQSnlGptgefJsiuYCdGEIJR9GnUUH1/0Wwm0I83DOHVoQ5udClzPlxYUGKn2i5986iv27+ENLRiGNkYxDWCUYpYhCELPbjCJOpxYhK04w4zuEQ6VpGihNGa8jHCMYYtpCLRjJKAWS3FbNAepvS/W7/+/zHqc4pltJlN3MAmblGkUVa0y2UKXNeGV+NJMzDJih2/asDuP55/nk/LyX33C1ZhLKpQzpCJccvxWZdgZneIJRVapEngRY0SnKNTa48zU1SgaV8gV0dqU4zXnMIDWkxRoBZ1GnlDFXmvNVdoT3yIQBWm/WvrVaImNKsazdMhNr1rVp8HxfzchQNEIR7zKNFmmdJ+VzElYw6N2FVCn/3XZerXbfRpvV59S1BQkDbdBH9YL9Nf19lcnhy929flCrjOXwIMANRqYeEhTz1jAAAAAElFTkSuQmCC" alt="Web Site" width="32" class="ltvl-sm-icon"/>
                        </a>
                      </div>
                      <button class="btn-apply" type="button" data-toggle="modal" data-target="#vanPost-<? the_ID(); ?>">Read More</button>
                    </div>
                  </div>
                </div>
              </div>
            </article>
            <div>
              <div id="vanPost-<? the_ID(); ?>" class="modal fade vanPost" tabindex="-1" role="dialog" aria-labelledby="vanPostLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <?php the_post_thumbnail( 'large', array( 'class' => 'img-vanpost' ) ); ?>
                            <div class="van-name">
                              <div>
                                <h3 class="text-uppercase ltvl-entry-title"><?php the_field('first_name'); ?></h3>
                                <h3 class="text-uppercase ltvl-entry-title"><?php the_field('last_name'); ?></h3>
                              </div>
                              <div>
                                <a class="link-ltvl" href="<?php the_field('ins_link'); ?>" target="blank">
                                  <img src="../wp-content/themes/gear-coop-blog/assets/images/i-black.png" alt="Instagram" width="32" class="ltvl-sm-icon"/>
                                </a>
                                <a class="link-ltvl" href="<?php the_field('fb_link'); ?>" target="blank">
                                  <img src="../wp-content/themes/gear-coop-blog/assets/images/fb-black.png" alt="Facebook" width="32" class="ltvl-sm-icon"/>
                                </a>
                                <a class="link-ltvl" href="<?php the_field('tw_link'); ?>" target="blank">
                                  <img src="../wp-content/themes/gear-coop-blog/assets/images/tw-black.png" alt="Twitter" width="32" class="ltvl-sm-icon"/>
                                </a>
                                <a class="link-ltvl" href="<?php the_field('www_link'); ?>" target="blank">
                                  <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAaNJREFUeNp000kohGEcx/GZwQxJHORkudgSWbJkyZKIlIssR+ukOLhw4SQ3ipNwcScuLnIQkUmWCBdcJlG2XIgw+D76jd4Mb32aZ973+f+f7f/YbYFPBsqRhwK928YO1nBk7RxkaTswgAZEohA3eEQSnlGptgefJsiuYCdGEIJR9GnUUH1/0Wwm0I83DOHVoQ5udClzPlxYUGKn2i5986iv27+ENLRiGNkYxDWCUYpYhCELPbjCJOpxYhK04w4zuEQ6VpGihNGa8jHCMYYtpCLRjJKAWS3FbNAepvS/W7/+/zHqc4pltJlN3MAmblGkUVa0y2UKXNeGV+NJMzDJih2/asDuP55/nk/LyX33C1ZhLKpQzpCJccvxWZdgZneIJRVapEngRY0SnKNTa48zU1SgaV8gV0dqU4zXnMIDWkxRoBZ1GnlDFXmvNVdoT3yIQBWm/WvrVaImNKsazdMhNr1rVp8HxfzchQNEIR7zKNFmmdJ+VzElYw6N2FVCn/3XZerXbfRpvV59S1BQkDbdBH9YL9Nf19lcnhy929flCrjOXwIMANRqYeEhTz1jAAAAAElFTkSuQmCC" alt="Web Site" width="32" class="ltvl-sm-icon"/>
                                </a>
                              </div>
                            </div>
                            <div class="van-content">
                              <?php the_content();?>
                            </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-cancel" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php endwhile; ?>
          <?php wp_reset_postdata(); ?>
          <?php echo do_shortcode('[ajax_load_more repeater="template_1" offset="3" post_type="vanlife" transition_container="false"]');?>
          <div class="col-md-12 col-pagination text-left col-sm-12 col-xs-12">
            <?php wp_bootstrap_pagination( array() ); ?>
          </div>
        </div>
      <?php else : ?>
        <p><?php _e( '', 'gearcoopblogtheme' ); ?></p>
      <?php endif; ?>
    </div>
  </div>
</div>
<?php get_footer(); ?>
