<!doctype html>
<!--  In times of universal deceit, telling the truth is a revolutionary act" - George Orwell-->
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="">
        <!-- Bootstrap core CSS -->
        <!-- Custom styles for this template -->
        <!-- Fonts -->
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-53568205-1', 'auto');
      ga('send', 'pageview');
    </script> -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-51807934-1', 'auto');
      ga('send', 'pageview');
    </script>
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php wp_head(); ?>
    </head>
    <body class="">
        <header class="">
            <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
            <?php $image_attributes = wp_get_attachment_image_src( get_theme_mod( 'jumbo_bkg' ), 'large' ); ?>
            <div class="jumbotron" style="<?php if($image_attributes) echo 'background-image:url(\''.$image_attributes[0].'\')' ?>">
                <div class="container text-center">
                    <?php echo wp_get_attachment_image( get_theme_mod( 'jumbo_logo' ), 'full', null, array(
                          'class' => 'jumbotron-img'
                    ) ) ?>
                    <h1 class="jumbotron-title"><?php echo get_theme_mod( 'jumbo_title', __( 'Hello, world!', 'gearcoopblogtheme' ) ); ?></h1>
                    <p class="jumbotron-text"><?php echo get_theme_mod( 'jumbo_desc', __( 'This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.', 'gearcoopblogtheme' ) ); ?></p>
                </div>
            </div>
            <nav id="navbar-main" class="navbar navbar-default menu menu-shylock" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only"><?php _e( 'Toggle navigation', 'gearcoopblogtheme' ); ?></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="http://blog.gearcoop.com" id="brand">
                            <img src="http://blog.gearcoop.com/wp-content/uploads/2016/03/logo.png" />
                        </a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <?php wp_nav_menu( array(
                              'menu' => 'primary',
                              'menu_class' => 'nav navbar-nav navbar-right',
                              'container' => '',
                              'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                              'walker' => new wp_bootstrap_navwalker()
                        ) ); ?>
                        <?php get_search_form( true ); ?>
                    </div>
                </div>
            </nav>
        </header>
        <main class="">
