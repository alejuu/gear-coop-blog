<?php
/*
Template Name: National Park Week Giveback
*/
?>
<?php
get_header('nationaldonate'); ?>

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <section class="post-header">
            <header class="u-centered u-bg-white">
                <div class="o-container featured-image">
                    <div class="o-container o-container--medium u-window-box--large u-absolute-center post-heading">
                      <div class="national-head">
                        <h4 class="u-color-white"><?php _e( 'We want to thank everyone for their contribution, and we hope you enjoy those custom stickers.', 'cooper' ); ?></h4>
                        <h1 class="u-color-white"><?php _e( 'Thank You', 'cooper' ); ?></h1>
                      </div>
                    </div>
                    <div class="featured-image-container u-absolute-center">
                        <?php the_post_thumbnail( null, array(
                              'class' => 'lazy-load o-image featured-image-img u-absolute-center'
                        ) ); ?>
                    </div>
                </div>
            </header>
        </section>
    <?php endwhile; ?>
<?php endif; ?>
<section class="u-bg-white post-content">
    <div class="o-container o-container--large u-window-box--large">
        <div class="u-window-box--medium">
            <?php the_content(); ?>
        </div>
    </div>
</section>
<script>
jQuery(document).ready(function($){
  $(".tab-input").click(function() {
    $('html, body').animate({
        scrollTop: $("#form-mobile-content").offset().top
    }, 1000);
  });
});
</script>
<?php get_footer(); ?>
