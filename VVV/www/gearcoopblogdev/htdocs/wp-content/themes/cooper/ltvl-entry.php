<?php
/*
Template Name: Live That Van Life 2 Entry
Template Post Type: vanlife
*/
?>
<?php
get_header('ltvl2-simple'); ?>

            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <section class="post-header">
                        <header class="u-centered u-bg-white">
                            <div class="u-window-box--large u-bg-white">
                                <h1><?php the_title(); ?></h1>
                                <div class="u-centered">
                                    <a class="link-ltvl c-link--black u-window-box--xsmall" href="<?php the_field('ins_link'); ?>" target="blank">
                                      <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </a>
                                    <a class="link-ltvl c-link--black u-window-box--xsmall" href="<?php the_field('fb_link'); ?>" target="blank">
                                      <i class="fa fa-facebook" aria-hidden="true"></i>
                                    </a>
                                    <a class="link-ltvl c-link--black u-window-box--xsmall" href="<?php the_field('tw_link'); ?>" target="blank">
                                      <i class="fa fa-twitter" aria-hidden="true"></i>
                                    </a>
                                    <a class="link-ltvl c-link--black u-window-box--xsmall" href="<?php the_field('www_link'); ?>" target="blank">
                                      <i class="fa fa-link" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="o-container o-container--medium u-window-box--large">
                                <?php the_post_thumbnail( null, array(
                                      'class' => 'lazy-load o-image'
                                ) ); ?>
                            </div>
                        </header>
                    </section>
                <?php endwhile; ?>
            <?php endif; ?>
            <section class="u-bg-white post-content">
                <div class="o-container o-container--medium u-window-box--large">
                    <div class="o-grid">
                        <div class="o-grid__cell">
                            <h6 class="u-color-grey"><?php the_date(); ?></h6>
                        </div>
                    </div>
                    <div class="u-window-box--medium">
                        <div>
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
            </section>
<?php get_footer('simple'); ?>
