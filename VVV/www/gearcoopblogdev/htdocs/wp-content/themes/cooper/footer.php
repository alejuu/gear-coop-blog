
        </main>
        <!--Main Content-->
        <!--Footer-->
        <footer class="main-footer">
            <nav>
                <ul class="c-nav c-nav--inline u-bg-grey-dark">
                    <li class="c-nav__item">
                        <a href="gearcoop.com" class="c-link--white"><?php _e( '©2017 Gear Coop', 'cooper' ); ?></a>
                    </li>
                    <li class="c-nav__item c-nav__item--right u-color-white">
                        <a class="c-link--white" href="https://www.gearcoop.com/contact_us"><?php _e( 'Contact', 'cooper' ); ?></a>
                    </li>
                    <li class="c-nav__item c-nav__item--right u-color-white">
                        <a href="https://twitter.com/gearcoop" class="c-link--white"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    </li>
                    <li class="c-nav__item c-nav__item--right u-color-white">
                        <a href="https://www.facebook.com/gearcoop" class="c-link--white"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    </li>
                    <li class="c-nav__item c-nav__item--right u-color-white">
                        <a href="https://www.instagram.com/gearcoop/" class="c-link--white"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    </li>
                </ul>
            </nav>
        </footer>
        <!--Footer-->
        <!-- <script src="js/anijs-min.js"></script>
    <script src="js/lazyload.min.js"></script>
    <script src="js/lity.min.js"></script>
    <script src="js/scripts.js"></script> -->
        <?php wp_footer(); ?>
    </body>
</html>
