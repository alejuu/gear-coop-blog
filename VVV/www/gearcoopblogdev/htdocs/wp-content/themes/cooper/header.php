<!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php wp_head(); ?>
    </head>
    <body>
        <!--Header-->
        <header class="u-bg-white main-header show-responsive show-medium--up">
            <?php ubermenu( 'topbar' , array( 'theme_location' => 'topbar' ) ); ?>
            <?php ubermenu( 'search' , array( 'theme_location' => 'secondary' ) ); ?>
            <?php ubermenu( 'main' , array( 'theme_location' => 'primary' ) ); ?>
        </header>
        <header class="u-bg-white main-header main-mobile-header hide-responsive hide-medium--up">
          <nav class="mobile-bar">
            <a href="https://www.gearcoop.com">
                <img class="lazy-load logo-mobile" data-src="/wp-content/themes/cooper/assets/Gear-Coop-logo-small.svg" data-src-mobile="/wp-content/themes/cooper/assets/Gear-Coop-logo-small.svg" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/placeholders/squares.svg" width="35">
            </a>
              <form id="searchform" role="search" method="get" class="c-input-group" action="<?php echo home_url( '/' ); ?>">
                <a class="c-button c-button--brand search-mobile-icon" href="#searchform-drop">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </a>
                <a class="c-button c-button--brand search-cart-icon" href="https://www.gearcoop.com/cart">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                </a>
                <div id="searchform-drop" class="o-field search-mobile">
                    <input type="search" class="search-field c-field input-search"
                                placeholder="<?php echo esc_attr_x( 'Search Posts', 'placeholder' ) ?>"
                                value="<?php echo get_search_query() ?>" name="s"
                                title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
                    <input type="submit" class="search-submit" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
                    <span class="fa fa-search"></span>
                </div>
            </form>
          </nav>
            <?php ubermenu( 'mobile' , array( 'theme_location' => 'mobile' ) ); ?>
        </header>
        <!--Header-->
        <!--Main Content-->
        <main>
