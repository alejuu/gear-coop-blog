<?php
/*
Template Name: Clean Page
*/
?>
<?php
get_header('simple'); ?>

            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>
                <?php endwhile; ?>
            <?php endif; ?>
            <section class="u-bg-white post-content">
                <div class="o-container o-container--medium u-window-box--large">
                    <div class="u-window-box--medium">
                        <div>
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
            </section>
            <style>
            .coopershare-social {
              display: none!important;
            }
            </style>
