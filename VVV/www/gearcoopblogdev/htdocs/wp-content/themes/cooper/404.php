<?php
get_header('404'); ?>

<section>
    <div class="o-container u-letter-box--large flex-box--center">
        <div class="o-container o-container--medium u-pillar-box--medium u-centered">
            <h1 class="u-color-white"><?php _e( 'Looks like you took the road less traveled.', 'cooper' ); ?></h1>
            <h4 class="u-color-white"><?php _e( 'We couldn\'t find the url you were looking for.', 'cooper' ); ?></h4>
            <a href="https://gearcoop.com" class="c-button c-button--inverted u-window-margin--small"><?php _e( 'Explore Gear Coop', 'cooper' ); ?></a>
            <a href="https://blog.gearcoop.com" class="c-button c-button--inverted u-window-margin--small"><?php _e( 'Discover Stories', 'cooper' ); ?></a>
            <a href="https://www.gearcoop.com/contact_us" class="c-button c-button--inverted u-window-margin--small"><?php _e( 'Contact Us', 'cooper' ); ?></a>
        </div>
    </div>
</section>

<?php get_footer(); ?>
