<?php
get_header(); ?>

            <section>
                <header class="u-centered u-bg-white">
                    <div class="u-window-box--large u-bg-white">
                        <?php echo get_avatar( get_the_author_meta( 'ID' ) ); ?>
                        <h1><?php the_author(); ?></h1>
                        <p><?php the_author_meta( 'user_description' ); ?></p>
                        <div class="author-icons">
                            <?php
    $twitter_profile = get_the_author_meta( 'twitter_profile' );
    if ( $twitter_profile && $twitter_profile != '' ) {
      echo '<a href="' . esc_url($twitter_profile) . '" rel="author" target="blank" class="h6 h-black-link">Twitter</a>';
    }
    $facebook_profile = get_the_author_meta( 'facebook_profile' );
      if ( $facebook_profile && $facebook_profile != '' ) {
        echo '<a href="' . esc_url($facebook_profile) . '" rel="author" target="blank" class="h6 h-black-link">Facebook</a>';
      }
    $instagram_profile = get_the_author_meta( 'instagram_profile' );
      if ( $instagram_profile && $instagram_profile != '' ) {
        echo '<a href="' . esc_url($instagram_profile) . '" rel="author" target="blank" class="h6 h-black-link">Instagram</a>';
      }
    $website_profile = get_the_author_meta( 'website_profile' );
      if ( $website_profile && $website_profile != '' ) {
        echo '<a href="' . esc_url($website_profile) . '" rel="author" target="blank" class="h6 h-black-link">Website</a>';
      }
  ?>
                        </div>
                    </div>
                </header>
            </section>
            <section>
                <?php if ( have_posts() ) : ?>
                    <div class="o-container o-container--large u-letter-box--medium">
                        <!--Grid-->
                        <div class="o-grid o-grid--small-full o-grid--medium-full flex-wrap">
                            <!--Col 1-->
                            <?php while ( have_posts() ) : the_post(); ?>
                                <div class="o-grid__cell o-grid__cell--width-33@medium u-letter-box--medium card-hover">
                                    <!--Card-->
                                    <div class="c-card">
                                        <div class="posts-thumb">
                                            <a href="<?php echo esc_url( wp_get_shortlink()); ?>"></a>
                                            <?php the_post_thumbnail( 'medium_large', array(
                                                  'class' => 'lazy-load o-image'
                                            ) ); ?>
                                        </div>
                                        <div class="u-window-box--small">
                                            <header class="c-card__header flex-box-column posts-header">
                                                <h4 class="c-heading__sub u-centered c-uppercase h-inside-black"><?php the_category( ' | ' ); ?></h4>
                                                <a class="c-heading u-centered h2 h-link flex-item--bottom" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php the_title(); ?></a>
                                                <h6 class="c-heading__sub u-centered flex-item--bottom h-inside-grey"><?php the_author_posts_link(); ?></h6>
                                            </header>
                                            <div class="c-card__body u-centered posts-body">
                                                <?php echo get_excerpt(100); ?>
                                            </div>
                                            <footer class="c-card__footer c-card__footer--block u-centered">
                                                <a class="c-link c-uppercase" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php _e( 'Read More', 'cooper' ); ?></a>
                                            </footer>
                                        </div>
                                    </div>
                                    <!--Card-->
                                </div>
                            <?php endwhile; ?>
                            <!--Col 1-->
                        </div>
                        <?php
if(is_author()){
   $author_id = get_the_author_meta('ID');
   $name = get_the_author_meta('display_name');
   echo do_shortcode('[ajax_load_more container_type="div" css_classes="o-grid o-grid--small-full o-grid--medium-full flex-wrap" repeater="template_2" post_type="post" posts_per_page="6" post_format="standard" author="'.$author_id.'" cache="true" offset="6" pause="true" scroll="false" transition_container="false" images_loaded="true"]');
}
?>
                        <!--Grid-->
                    </div>
                <?php else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.', 'cooper' ); ?></p>
                <?php endif; ?>
            </section>            

<?php get_footer(); ?>