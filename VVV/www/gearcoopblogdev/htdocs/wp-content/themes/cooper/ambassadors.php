<?php
/*
 Template Name: Ambassadors
 */
?>
<?php
get_header(); ?>

            <!--Featured Image-->
            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <section class="post-header">
                        <header class="u-centered u-bg-white">
                            <div class="o-container featured-image">
                                <div class="o-container o-container--medium u-window-box--large u-absolute-center post-heading">
                                    <h1 class="u-color-white"><?php the_title(); ?></h1>
                                </div>
                                <div class="featured-image-container u-absolute-center">
                                    <?php the_post_thumbnail( null, array(
                                          'class' => 'lazy-load o-image featured-image-img u-absolute-center'
                                    ) ); ?>
                                </div>
                            </div>
                        </header>
                    </section>
                <?php endwhile; ?>
            <?php endif; ?>
            <!--Featured Image-->
            <!--Tabs-->
            <section class="post-content u-bg-grey-light">
                <!--Full Container-->
                <div class="o-container">
                    <!--Tabs BG-->
                    <div class="u-bg-white w-full tabs-menu tabs-ambassadors">
</div>
                    <!--Tabs BG-->
                    <!--Container-->
                    <div class="o-container o-container--large home-tabs">
                        <div class="c-tabs u-centered">
                            <!--Tab Headings-->
                            <div class="c-tabs__headings tab-frame flex-wrap">
                                <input type="radio" checked name="tab" id="home-tab--1">
                                <label class="c-tab-heading h3" for="home-tab--1" data-anijs="if: click, do: fade-in animated, to: #tab-1--content">
                                    <?php _e( 'All', 'cooper' ); ?>
                                </label>
                                <input type="radio" name="tab" id="home-tab--2">
                                <label class="c-tab-heading h3" for="home-tab--2" data-anijs="if: click, do: fade-in animated, to: #tab-2--content">
                                    <?php _e( 'Becca Bredehoft', 'cooper' ); ?>
                                </label>
                                <input type="radio" name="tab" id="home-tab--3">
                                <label class="c-tab-heading h3" for="home-tab--3" data-anijs="if: click, do: fade-in animated, to: #tab-3--content">
                                    <?php _e( 'Nayton Rosales', 'cooper' ); ?>
                                </label>
                                <input type="radio" name="tab" id="home-tab--4">
                                <label class="c-tab-heading h3" for="home-tab--4" data-anijs="if: click, do: fade-in animated, to: #tab-4--content">
                                    <?php _e( 'Jake Carpenter', 'cooper' ); ?>
                                </label>
                                <input type="radio" name="tab" id="home-tab--5">
                                <label class="c-tab-heading h3" for="home-tab--5" data-anijs="if: click, do: fade-in animated, to: #tab-5--content">
                                    <?php _e( 'Kayla King', 'cooper' ); ?>
                                </label>
                                <input type="radio" name="tab" id="home-tab--6">
                                <label class="c-tab-heading h3" for="home-tab--6" data-anijs="if: click, do: fade-in animated, to: #tab-6--content">
                                    <?php _e( 'Corey Cosby', 'cooper' ); ?>
                                </label>
                                <!--Tab Content 1-->
                                <div id="tab-1--content" class="c-tabs__tab tab-content">
                                    <!--Container Card-->
                                    <div class="o-container o-container--xlarge u-window-box--medium card-hover">
                                        <?php
                                            $featuredambassadors_args = array(
                                              'cat' => 'ambassadors',
                                              'category_name' => 'ambassadors',
                                              'post_type' => 'post'
                                            )
                                        ?>
                                        <?php $featuredambassadors = new WP_Query( $featuredambassadors_args ); ?>
                                        <?php if ( $featuredambassadors->have_posts() ) : ?>
                                            <div class="c-card w-full">
                                                <?php $featuredambassadors_item_number = 0; ?>
                                                <?php while ( $featuredambassadors->have_posts() && $featuredambassadors_item_number++ < 1 ) : $featuredambassadors->the_post(); ?>
                                                    <div class="c-card__item u-window-box--none">
                                                        <!--Grid-->
                                                        <div class="o-grid o-grid--small-full o-grid--medium-full">
                                                            <!--Col 1-->
                                                            <div class="o-grid__cell o-grid__cell--featured-1 u-window-box--none">
                                                                <div class="posts-featured">
                                                                    <a href="<?php echo esc_url( wp_get_shortlink()); ?>"></a>
                                                                    <?php the_post_thumbnail( 'medium_large', array(
                                                                          'class' => 'lazy-load o-image'
                                                                    ) ); ?>
                                                                    <!--Featured Img-->
                                                                    <!--Featured Img-->
                                                                </div>
                                                            </div>
                                                            <!--Col 1-->
                                                            <!--Col 2-->
                                                            <div class="o-grid__cell o-grid__cell--featured-2 flex-box-column u-left u-letter-box--medium">
                                                                <header class="c-card__header">
                                                                    <h3 class="c-heading__sub c-uppercase"><?php _e( 'Featured', 'cooper' ); ?></h3>
                                                                </header>
                                                                <div class="c-card__body flex-item--bottom">
                                                                    <header class="c-card__header u-pillar-box--none">
                                                                        <h4 class="c-heading__sub c-uppercase h-inside-black"><?php the_category( ' | ' ); ?></h4>
                                                                        <a class="c-heading h2 h-link" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php the_title(); ?></a>
                                                                        <h6 class="c-heading__sub h-inside-grey"><?php the_author_posts_link(); ?></h6>
                                                                    </header>
                                                                    <?php echo get_excerpt(150); ?>
                                                                </div>
                                                                <footer class="c-card__footer c-card__footer--block flex-item--bottom">
                                                                    <a class="c-link" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php _e( 'Read More', 'cooper' ); ?></a>
                                                                </footer>
                                                            </div>
                                                            <!--Col 2-->
                                                        </div>
                                                        <!--Grid-->
                                                    </div>
                                                <?php endwhile; ?>
                                                <?php wp_reset_postdata(); ?>
                                            </div>
                                        <?php endif; ?>
                                        <!--Card-->
                                        <!--Card-->
                                    </div>
                                    <!--Container Card-->
                                    <!--Container Cards-->
                                    <?php
                                        $allambassadors_args = array(
                                          'cat' => 'ambassadors',
                                          'category_name' => 'ambassadors',
                                          'offset' => '1'
                                        )
                                    ?>
                                    <?php $allambassadors = new WP_Query( $allambassadors_args ); ?>
                                    <?php if ( $allambassadors->have_posts() ) : ?>
                                        <div class="o-container o-container--large">
                                            <!--Grid-->
                                            <div class="o-grid o-grid--small-full o-grid--medium-full flex-wrap">
                                                <!--Col 1-->
                                                <?php while ( $allambassadors->have_posts() ) : $allambassadors->the_post(); ?>
                                                    <div class="o-grid__cell o-grid__cell--width-33@medium u-letter-box--medium card-hover">
                                                        <!--Card-->
                                                        <div class="c-card">
                                                            <div class="posts-thumb">
                                                                <a href="<?php echo esc_url( wp_get_shortlink()); ?>"></a>
                                                                <?php the_post_thumbnail( 'medium_large', array(
                                                                      'class' => 'lazy-load o-image'
                                                                ) ); ?>
                                                            </div>
                                                            <div class="u-window-box--small">
                                                                <header class="c-card__header flex-box-column posts-header">
                                                                    <h4 class="c-heading__sub u-centered c-uppercase h-inside-black"><?php the_category( ' | ' ); ?></h4>
                                                                    <a class="c-heading u-centered h2 h-link flex-item--bottom" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php the_title(); ?></a>
                                                                    <h6 class="c-heading__sub u-centered flex-item--bottom h-inside-grey"><?php the_author_posts_link(); ?></h6>
                                                                </header>
                                                                <div class="c-card__body u-centered posts-body">
                                                                    <?php echo get_excerpt(100); ?>
                                                                </div>
                                                                <footer class="c-card__footer c-card__footer--block u-centered">
                                                                    <a class="c-link c-uppercase" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php _e( 'Read More', 'cooper' ); ?></a>
                                                                </footer>
                                                            </div>
                                                            <!--Card Image-->
                                                            <!--Card Image-->
                                                            <!--Card Header-->
                                                            <!--Card Header-->
                                                            <!--Card Body-->
                                                            <!--Card Body-->
                                                            <!--Card Footer-->
                                                            <!--Card Footer-->
                                                        </div>
                                                        <!--Card-->
                                                    </div>
                                                <?php endwhile; ?>
                                                <?php wp_reset_postdata(); ?>
                                                <!--Col 1-->
                                            </div>
                                            <?php echo do_shortcode('[ajax_load_more container_type="div" css_classes="o-grid o-grid--small-full o-grid--medium-full flex-wrap" repeater="template_2" post_type="post" category="ambassadors" posts_per_page="6" post_format="standard" cache="true" offset="7" pause="true" scroll="false" transition_container="false" images_loaded="true"]'); ?>
                                            <!--Grid-->
                                        </div>
                                    <?php endif; ?>
                                    <!--Container Cards-->
                                </div>
                                <!--Tab Content 1-->
                                <!--Tab Content 2-->
                                <div id="tab-2--content" class="c-tabs__tab tab-content">
                                    <!--Container Card-->
                                    <div class="o-container o-container--xlarge u-window-box--medium">
                                        <?php
                                            $becca_args = array(
                                              'author_name' => 'Becca'
                                            )
                                        ?>
                                        <?php $becca = new WP_Query( $becca_args ); ?>
                                        <?php if ( $becca->have_posts() ) : ?>
                                            <div class="c-card w-full">
                                                <?php $becca_item_number = 0; ?>
                                                <?php while ( $becca->have_posts() && $becca_item_number++ < 1 ) : $becca->the_post(); ?>
                                                    <div class="c-card__item u-window-box--none">
                                                        <!--Grid-->
                                                        <div class="o-grid o-grid--small-full o-grid--medium-full">
                                                            <!--Col 2-->
                                                            <div class="o-grid__cell flex-box-column u-left u-letter-box--medium">
                                                                <header class="c-card__header">
                                                                    <h3 class="c-heading__sub c-uppercase"><?php _e( 'Ambassador', 'cooper' ); ?></h3>
                                                                    <?php echo get_avatar( get_the_author_meta( 'ID' ) ); ?>
                                                                </header>
                                                                <div class="c-card__body flex-item--bottom">
                                                                    <header class="c-card__header u-pillar-box--none">
                                                                        <h6 class="c-heading h2 h-inside-black"><?php the_author_posts_link(); ?></h6>
                                                                    </header>
                                                                    <div class="author-icons">
                                                                        <?php
                      $twitter_profile = get_the_author_meta( 'twitter_profile' );
                      if ( $twitter_profile && $twitter_profile != '' ) {
                        echo '<a href="' . esc_url($twitter_profile) . '" rel="author" target="blank" class="h6 h-black-link">Twitter</a>';
                      }
                      $facebook_profile = get_the_author_meta( 'facebook_profile' );
                        if ( $facebook_profile && $facebook_profile != '' ) {
                          echo '<a href="' . esc_url($facebook_profile) . '" rel="author" target="blank" class="h6 h-black-link">Facebook</a>';
                        }
                      $instagram_profile = get_the_author_meta( 'instagram_profile' );
                        if ( $instagram_profile && $instagram_profile != '' ) {
                          echo '<a href="' . esc_url($instagram_profile) . '" rel="author" target="blank" class="h6 h-black-link">Instagram</a>';
                        }
                      $website_profile = get_the_author_meta( 'website_profile' );
                        if ( $website_profile && $website_profile != '' ) {
                          echo '<a href="' . esc_url($website_profile) . '" rel="author" target="blank" class="h6 h-black-link">Website</a>';
                        }
                    ?>
                                                                    </div>
                                                                    <p class="c-text"><?php the_author_meta( 'description' ); ?></p>
                                                                </div>
                                                                <footer class="c-card__footer c-card__footer--block flex-item--bottom">
</footer>
                                                            </div>
                                                            <!--Col 2-->
                                                        </div>
                                                        <!--Grid-->
                                                    </div>
                                                <?php endwhile; ?>
                                                <?php wp_reset_postdata(); ?>
                                            </div>
                                        <?php endif; ?>
                                        <!--Card-->
                                        <!--Card-->
                                    </div>
                                    <!--Container Card-->
                                    <!--Container Cards-->
                                    <?php
                                        $beccaall_args = array(
                                          'author_name' => 'Becca'
                                        )
                                    ?>
                                    <?php $beccaall = new WP_Query( $beccaall_args ); ?>
                                    <?php if ( $beccaall->have_posts() ) : ?>
                                        <div class="o-container o-container--large">
                                            <!--Grid-->
                                            <div class="o-grid o-grid--small-full o-grid--medium-full flex-wrap">
                                                <!--Col 1-->
                                                <?php while ( $beccaall->have_posts() ) : $beccaall->the_post(); ?>
                                                    <div class="o-grid__cell o-grid__cell--width-33@medium u-letter-box--medium card-hover">
                                                        <!--Card-->
                                                        <div class="c-card">
                                                            <div class="posts-thumb">
                                                                <a href="<?php echo esc_url( wp_get_shortlink()); ?>"></a>
                                                                <?php the_post_thumbnail( 'medium_large', array(
                                                                      'class' => 'lazy-load o-image'
                                                                ) ); ?>
                                                            </div>
                                                            <div class="u-window-box--small">
                                                                <header class="c-card__header flex-box-column posts-header">
                                                                    <h4 class="c-heading__sub u-centered c-uppercase h-inside-black"><?php the_category( ' | ' ); ?></h4>
                                                                    <a class="c-heading u-centered h2 h-link flex-item--bottom" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php the_title(); ?></a>
                                                                    <h6 class="c-heading__sub u-centered flex-item--bottom h-inside-grey"><?php the_author_posts_link(); ?></h6>
                                                                </header>
                                                                <div class="c-card__body u-centered posts-body">
                                                                    <?php echo get_excerpt(100); ?>
                                                                </div>
                                                                <footer class="c-card__footer c-card__footer--block u-centered">
                                                                    <a class="c-link c-uppercase" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php _e( 'Read More', 'cooper' ); ?></a>
                                                                </footer>
                                                            </div>
                                                            <!--Card Image-->
                                                            <!--Card Image-->
                                                            <!--Card Header-->
                                                            <!--Card Header-->
                                                            <!--Card Body-->
                                                            <!--Card Body-->
                                                            <!--Card Footer-->
                                                            <!--Card Footer-->
                                                        </div>
                                                        <!--Card-->
                                                    </div>
                                                <?php endwhile; ?>
                                                <?php wp_reset_postdata(); ?>
                                                <!--Col 1-->
                                                <!--Col 2-->
                                                <!--Col 2-->
                                                <!--Col 3-->
                                                <!--Col 3-->
                                            </div>
                                            <?php echo do_shortcode('[ajax_load_more container_type="div" css_classes="o-grid o-grid--small-full o-grid--medium-full flex-wrap" repeater="template_2" author="18" post_type="post" posts_per_page="6" post_format="standard" cache="true" offset="6" pause="true" scroll="false" transition_container="false" images_loaded="true"]'); ?>
                                            <!--Grid-->
                                        </div>
                                    <?php endif; ?>
                                    <!--Container Cards-->
                                </div>
                                <!--Tab Content 2-->
                                <!--Tab Content 3-->
                                <div id="tab-3--content" class="c-tabs__tab tab-content">
                                    <!--Container Card-->
                                    <div class="o-container o-container--xlarge u-window-box--medium">
                                        <?php
                                            $nayton_args = array(
                                              'author_name' => 'Nayton'
                                            )
                                        ?>
                                        <?php $nayton = new WP_Query( $nayton_args ); ?>
                                        <?php if ( $nayton->have_posts() ) : ?>
                                            <div class="c-card w-full">
                                                <?php $nayton_item_number = 0; ?>
                                                <?php while ( $nayton->have_posts() && $nayton_item_number++ < 1 ) : $nayton->the_post(); ?>
                                                    <div class="c-card__item u-window-box--none">
                                                        <!--Grid-->
                                                        <div class="o-grid o-grid--small-full o-grid--medium-full">
                                                            <!--Col 1-->
                                                            <div class="o-grid__cell flex-box-column u-left u-letter-box--medium">
                                                                <header class="c-card__header">
                                                                    <h3 class="c-heading__sub c-uppercase"><?php _e( 'Ambassador', 'cooper' ); ?></h3>
                                                                    <?php echo get_avatar( get_the_author_meta( 'ID' ) ); ?>
                                                                </header>
                                                                <div class="c-card__body flex-item--bottom">
                                                                    <header class="c-card__header u-pillar-box--none">
                                                                        <h6 class="c-heading h2 h-inside-black"><?php the_author_posts_link(); ?></h6>
                                                                    </header>
                                                                    <div class="author-icons">
                                                                        <?php
                      $twitter_profile = get_the_author_meta( 'twitter_profile' );
                      if ( $twitter_profile && $twitter_profile != '' ) {
                        echo '<a href="' . esc_url($twitter_profile) . '" rel="author" target="blank" class="h6 h-black-link">Twitter</a>';
                      }
                      $facebook_profile = get_the_author_meta( 'facebook_profile' );
                        if ( $facebook_profile && $facebook_profile != '' ) {
                          echo '<a href="' . esc_url($facebook_profile) . '" rel="author" target="blank" class="h6 h-black-link">Facebook</a>';
                        }
                      $instagram_profile = get_the_author_meta( 'instagram_profile' );
                        if ( $instagram_profile && $instagram_profile != '' ) {
                          echo '<a href="' . esc_url($instagram_profile) . '" rel="author" target="blank" class="h6 h-black-link">Instagram</a>';
                        }
                      $website_profile = get_the_author_meta( 'website_profile' );
                        if ( $website_profile && $website_profile != '' ) {
                          echo '<a href="' . esc_url($website_profile) . '" rel="author" target="blank" class="h6 h-black-link">Website</a>';
                        }
                    ?>
                                                                    </div>
                                                                    <p class="c-text"><?php the_author_meta( 'description' ); ?></p>
                                                                </div>
                                                                <footer class="c-card__footer c-card__footer--block flex-item--bottom">
</footer>
                                                            </div>
                                                            <!--Col 1-->
                                                        </div>
                                                        <!--Grid-->
                                                    </div>
                                                <?php endwhile; ?>
                                                <?php wp_reset_postdata(); ?>
                                            </div>
                                        <?php endif; ?>
                                        <!--Card-->
                                        <!--Card-->
                                    </div>
                                    <!--Container Card-->
                                    <!--Container Cards-->
                                    <?php
                                        $naytonall_args = array(
                                          'author_name' => 'Nayton'
                                        )
                                    ?>
                                    <?php $naytonall = new WP_Query( $naytonall_args ); ?>
                                    <?php if ( $naytonall->have_posts() ) : ?>
                                        <div class="o-container o-container--large">
                                            <!--Grid-->
                                            <div class="o-grid o-grid--small-full o-grid--medium-full flex-wrap">
                                                <!--Col 1-->
                                                <?php while ( $naytonall->have_posts() ) : $naytonall->the_post(); ?>
                                                    <div class="o-grid__cell o-grid__cell--width-33@medium u-letter-box--medium card-hover">
                                                        <!--Card-->
                                                        <div class="c-card">
                                                            <div class="posts-thumb">
                                                                <a href="<?php echo esc_url( wp_get_shortlink()); ?>"></a>
                                                                <?php the_post_thumbnail( 'medium_large', array(
                                                                      'class' => 'lazy-load o-image'
                                                                ) ); ?>
                                                            </div>
                                                            <div class="u-window-box--small">
                                                                <header class="c-card__header flex-box-column posts-header">
                                                                    <h4 class="c-heading__sub u-centered c-uppercase h-inside-black"><?php the_category( ' | ' ); ?></h4>
                                                                    <a class="c-heading u-centered h2 h-link flex-item--bottom" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php the_title(); ?></a>
                                                                    <h6 class="c-heading__sub u-centered flex-item--bottom h-inside-grey"><?php the_author_posts_link(); ?></h6>
                                                                </header>
                                                                <div class="c-card__body u-centered posts-body">
                                                                    <?php echo get_excerpt(100); ?>
                                                                </div>
                                                                <footer class="c-card__footer c-card__footer--block u-centered">
                                                                    <a class="c-link c-uppercase" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php _e( 'Read More', 'cooper' ); ?></a>
                                                                </footer>
                                                            </div>
                                                            <!--Card Image-->
                                                            <!--Card Image-->
                                                            <!--Card Header-->
                                                            <!--Card Header-->
                                                            <!--Card Body-->
                                                            <!--Card Body-->
                                                            <!--Card Footer-->
                                                            <!--Card Footer-->
                                                        </div>
                                                        <!--Card-->
                                                    </div>
                                                <?php endwhile; ?>
                                                <?php wp_reset_postdata(); ?>
                                                <!--Col 1-->
                                                <!--Col 2-->
                                                <!--Col 2-->
                                                <!--Col 3-->
                                                <!--Col 3-->
                                            </div>
                                            <?php echo do_shortcode('[ajax_load_more container_type="div" css_classes="o-grid o-grid--small-full o-grid--medium-full flex-wrap" repeater="template_2" author="22" post_type="post" posts_per_page="6" post_format="standard" cache="true" offset="6" pause="true" scroll="false" transition_container="false" images_loaded="true"]'); ?>
                                            <!--Grid-->
                                        </div>
                                    <?php endif; ?>
                                    <!--Container Cards-->
                                </div>
                                <!--Tab Content 3-->
                                <!--Tab Content 4-->
                                <div id="tab-4--content" class="c-tabs__tab tab-content">
                                    <!--Container Card-->
                                    <div class="o-container o-container--xlarge u-window-box--medium">
                                        <?php
                                            $jake_args = array(
                                              'author_name' => 'JCarp'
                                            )
                                        ?>
                                        <?php $jake = new WP_Query( $jake_args ); ?>
                                        <?php if ( $jake->have_posts() ) : ?>
                                            <div class="c-card w-full">
                                                <?php $jake_item_number = 0; ?>
                                                <?php while ( $jake->have_posts() && $jake_item_number++ < 1 ) : $jake->the_post(); ?>
                                                    <div class="c-card__item u-window-box--none">
                                                        <!--Grid-->
                                                        <div class="o-grid o-grid--small-full o-grid--medium-full">
                                                            <!--Col 1-->
                                                            <div class="o-grid__cell flex-box-column u-left u-letter-box--medium">
                                                                <header class="c-card__header">
                                                                    <h3 class="c-heading__sub c-uppercase"><?php _e( 'Ambassador', 'cooper' ); ?></h3>
                                                                    <?php echo get_avatar( get_the_author_meta( 'ID' ) ); ?>
                                                                </header>
                                                                <div class="c-card__body flex-item--bottom">
                                                                    <header class="c-card__header u-pillar-box--none">
                                                                        <h6 class="c-heading h2 h-inside-black"><?php the_author_posts_link(); ?></h6>
                                                                    </header>
                                                                    <div class="author-icons">
                                                                        <?php
                      $twitter_profile = get_the_author_meta( 'twitter_profile' );
                      if ( $twitter_profile && $twitter_profile != '' ) {
                        echo '<a href="' . esc_url($twitter_profile) . '" rel="author" target="blank" class="h6 h-black-link">Twitter</a>';
                      }
                      $facebook_profile = get_the_author_meta( 'facebook_profile' );
                        if ( $facebook_profile && $facebook_profile != '' ) {
                          echo '<a href="' . esc_url($facebook_profile) . '" rel="author" target="blank" class="h6 h-black-link">Facebook</a>';
                        }
                      $instagram_profile = get_the_author_meta( 'instagram_profile' );
                        if ( $instagram_profile && $instagram_profile != '' ) {
                          echo '<a href="' . esc_url($instagram_profile) . '" rel="author" target="blank" class="h6 h-black-link">Instagram</a>';
                        }
                      $website_profile = get_the_author_meta( 'website_profile' );
                        if ( $website_profile && $website_profile != '' ) {
                          echo '<a href="' . esc_url($website_profile) . '" rel="author" target="blank" class="h6 h-black-link">Website</a>';
                        }
                    ?>
                                                                    </div>
                                                                    <p class="c-text"><?php the_author_meta( 'description' ); ?></p>
                                                                </div>
                                                                <footer class="c-card__footer c-card__footer--block flex-item--bottom">
</footer>
                                                            </div>
                                                            <!--Col 1-->
                                                        </div>
                                                        <!--Grid-->
                                                    </div>
                                                <?php endwhile; ?>
                                                <?php wp_reset_postdata(); ?>
                                            </div>
                                        <?php endif; ?>
                                        <!--Card-->
                                        <!--Card-->
                                    </div>
                                    <!--Container Card-->
                                    <!--Container Cards-->
                                    <?php
                                        $jackesall_args = array(
                                          'author_name' => 'JCarp'
                                        )
                                    ?>
                                    <?php $jackesall = new WP_Query( $jackesall_args ); ?>
                                    <?php if ( $jackesall->have_posts() ) : ?>
                                        <div class="o-container o-container--large">
                                            <!--Grid-->
                                            <div class="o-grid o-grid--small-full o-grid--medium-full flex-wrap">
                                                <!--Col 1-->
                                                <?php while ( $jackesall->have_posts() ) : $jackesall->the_post(); ?>
                                                    <div class="o-grid__cell o-grid__cell--width-33@medium u-letter-box--medium card-hover">
                                                        <!--Card-->
                                                        <div class="c-card">
                                                            <div class="posts-thumb">
                                                                <a href="<?php echo esc_url( wp_get_shortlink()); ?>"></a>
                                                                <?php the_post_thumbnail( 'medium_large', array(
                                                                      'class' => 'lazy-load o-image'
                                                                ) ); ?>
                                                            </div>
                                                            <div class="u-window-box--small">
                                                                <header class="c-card__header flex-box-column posts-header">
                                                                    <h4 class="c-heading__sub u-centered c-uppercase h-inside-black"><?php the_category( ' | ' ); ?></h4>
                                                                    <a class="c-heading u-centered h2 h-link flex-item--bottom" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php the_title(); ?></a>
                                                                    <h6 class="c-heading__sub u-centered flex-item--bottom h-inside-grey"><?php the_author_posts_link(); ?></h6>
                                                                </header>
                                                                <div class="c-card__body u-centered posts-body">
                                                                    <?php echo get_excerpt(100); ?>
                                                                </div>
                                                                <footer class="c-card__footer c-card__footer--block u-centered">
                                                                    <a class="c-link c-uppercase" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php _e( 'Read More', 'cooper' ); ?></a>
                                                                </footer>
                                                            </div>
                                                            <!--Card Image-->
                                                            <!--Card Image-->
                                                            <!--Card Header-->
                                                            <!--Card Header-->
                                                            <!--Card Body-->
                                                            <!--Card Body-->
                                                            <!--Card Footer-->
                                                            <!--Card Footer-->
                                                        </div>
                                                        <!--Card-->
                                                    </div>
                                                <?php endwhile; ?>
                                                <?php wp_reset_postdata(); ?>
                                                <!--Col 1-->
                                            </div>
                                            <?php echo do_shortcode('[ajax_load_more container_type="div" css_classes="o-grid o-grid--small-full o-grid--medium-full flex-wrap" repeater="template_2" author="24" post_type="post" posts_per_page="6" post_format="standard" cache="true" offset="6" pause="true" scroll="false" transition_container="false" images_loaded="true"]'); ?>
                                            <!--Grid-->
                                        </div>
                                    <?php endif; ?>
                                    <!--Container Cards-->
                                </div>
                                <!--Tab Content 4-->
                                <!--Tab Content 5-->
                                <div id="tab-5--content" class="c-tabs__tab tab-content">
                                    <!--Container Card-->
                                    <div class="o-container o-container--xlarge u-window-box--medium">
                                        <?php
                                            $kayla_args = array(
                                              'author_name' => 'Kayla'
                                            )
                                        ?>
                                        <?php $kayla = new WP_Query( $kayla_args ); ?>
                                        <?php if ( $kayla->have_posts() ) : ?>
                                            <div class="c-card w-full">
                                                <?php $kayla_item_number = 0; ?>
                                                <?php while ( $kayla->have_posts() && $kayla_item_number++ < 1 ) : $kayla->the_post(); ?>
                                                    <div class="c-card__item u-window-box--none">
                                                        <!--Grid-->
                                                        <div class="o-grid o-grid--small-full o-grid--medium-full">
                                                            <!--Col 1-->
                                                            <div class="o-grid__cell flex-box-column u-left u-letter-box--medium">
                                                                <header class="c-card__header">
                                                                    <h3 class="c-heading__sub c-uppercase"><?php _e( 'Ambassador', 'cooper' ); ?></h3>
                                                                    <?php echo get_avatar( get_the_author_meta( 'ID' ) ); ?>
                                                                </header>
                                                                <div class="c-card__body flex-item--bottom">
                                                                    <header class="c-card__header u-pillar-box--none">
                                                                        <h6 class="c-heading h2 h-inside-black"><?php the_author_posts_link(); ?></h6>
                                                                    </header>
                                                                    <div class="author-icons">
                                                                        <?php
                      $twitter_profile = get_the_author_meta( 'twitter_profile' );
                      if ( $twitter_profile && $twitter_profile != '' ) {
                        echo '<a href="' . esc_url($twitter_profile) . '" rel="author" target="blank" class="h6 h-black-link">Twitter</a>';
                      }
                      $facebook_profile = get_the_author_meta( 'facebook_profile' );
                        if ( $facebook_profile && $facebook_profile != '' ) {
                          echo '<a href="' . esc_url($facebook_profile) . '" rel="author" target="blank" class="h6 h-black-link">Facebook</a>';
                        }
                      $instagram_profile = get_the_author_meta( 'instagram_profile' );
                        if ( $instagram_profile && $instagram_profile != '' ) {
                          echo '<a href="' . esc_url($instagram_profile) . '" rel="author" target="blank" class="h6 h-black-link">Instagram</a>';
                        }
                      $website_profile = get_the_author_meta( 'website_profile' );
                        if ( $website_profile && $website_profile != '' ) {
                          echo '<a href="' . esc_url($website_profile) . '" rel="author" target="blank" class="h6 h-black-link">Website</a>';
                        }
                    ?>
                                                                    </div>
                                                                    <p class="c-text"><?php the_author_meta( 'description' ); ?></p>
                                                                </div>
                                                                <footer class="c-card__footer c-card__footer--block flex-item--bottom">
</footer>
                                                            </div>
                                                            <!--Col 1-->
                                                        </div>
                                                        <!--Grid-->
                                                    </div>
                                                <?php endwhile; ?>
                                                <?php wp_reset_postdata(); ?>
                                            </div>
                                        <?php endif; ?>
                                        <!--Card-->
                                        <!--Card-->
                                    </div>
                                    <!--Container Card-->
                                    <!--Container Cards-->
                                    <?php
                                        $kaylaall_args = array(
                                          'author_name' => 'Kayla'
                                        )
                                    ?>
                                    <?php $kaylaall = new WP_Query( $kaylaall_args ); ?>
                                    <?php if ( $kaylaall->have_posts() ) : ?>
                                        <div class="o-container o-container--large">
                                            <!--Grid-->
                                            <div class="o-grid o-grid--small-full o-grid--medium-full flex-wrap">
                                                <!--Col 1-->
                                                <?php while ( $kaylaall->have_posts() ) : $kaylaall->the_post(); ?>
                                                    <div class="o-grid__cell o-grid__cell--width-33@medium u-letter-box--medium card-hover">
                                                        <!--Card-->
                                                        <div class="c-card">
                                                            <div class="posts-thumb">
                                                                <a href="<?php echo esc_url( wp_get_shortlink()); ?>"></a>
                                                                <?php the_post_thumbnail( 'medium_large', array(
                                                                      'class' => 'lazy-load o-image'
                                                                ) ); ?>
                                                            </div>
                                                            <div class="u-window-box--small">
                                                                <header class="c-card__header flex-box-column posts-header">
                                                                    <h4 class="c-heading__sub u-centered c-uppercase h-inside-black"><?php the_category( ' | ' ); ?></h4>
                                                                    <a class="c-heading u-centered h2 h-link flex-item--bottom" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php the_title(); ?></a>
                                                                    <h6 class="c-heading__sub u-centered flex-item--bottom h-inside-grey"><?php the_author_posts_link(); ?></h6>
                                                                </header>
                                                                <div class="c-card__body u-centered posts-body">
                                                                    <?php echo get_excerpt(100); ?>
                                                                </div>
                                                                <footer class="c-card__footer c-card__footer--block u-centered">
                                                                    <a class="c-link c-uppercase" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php _e( 'Read More', 'cooper' ); ?></a>
                                                                </footer>
                                                            </div>
                                                            <!--Card Image-->
                                                            <!--Card Image-->
                                                            <!--Card Header-->
                                                            <!--Card Header-->
                                                            <!--Card Body-->
                                                            <!--Card Body-->
                                                            <!--Card Footer-->
                                                            <!--Card Footer-->
                                                        </div>
                                                        <!--Card-->
                                                    </div>
                                                <?php endwhile; ?>
                                                <?php wp_reset_postdata(); ?>
                                                <!--Col 1-->
                                                <!--Col 2-->
                                                <!--Col 2-->
                                                <!--Col 3-->
                                                <!--Col 3-->
                                            </div>
                                            <?php echo do_shortcode('[ajax_load_more container_type="div" css_classes="o-grid o-grid--small-full o-grid--medium-full flex-wrap" repeater="template_2" author="16" post_type="post" posts_per_page="6" post_format="standard" cache="true" offset="6" pause="true" scroll="false" transition_container="false" images_loaded="true"]'); ?>
                                            <!--Grid-->
                                        </div>
                                    <?php endif; ?>
                                    <!--Container Cards-->
                                </div>
                                <!--Tab Content 5-->
                                <!--Tab Content 6-->
                                <div id="tab-6--content" class="c-tabs__tab tab-content">
                                    <!--Container Card-->
                                    <div class="o-container o-container--xlarge u-window-box--medium">
                                        <?php
                                            $corey_args = array(
                                              'author_name' => 'Corey'
                                            )
                                        ?>
                                        <?php $corey = new WP_Query( $corey_args ); ?>
                                        <?php if ( $corey->have_posts() ) : ?>
                                            <div class="c-card w-full">
                                                <?php $corey_item_number = 0; ?>
                                                <?php while ( $corey->have_posts() && $corey_item_number++ < 1 ) : $corey->the_post(); ?>
                                                    <div class="c-card__item u-window-box--none">
                                                        <!--Grid-->
                                                        <div class="o-grid o-grid--small-full o-grid--medium-full">
                                                            <!--Col 1-->
                                                            <div class="o-grid__cell flex-box-column u-left u-letter-box--medium">
                                                                <header class="c-card__header">
                                                                    <h3 class="c-heading__sub c-uppercase"><?php _e( 'Ambassador', 'cooper' ); ?></h3>
                                                                    <?php echo get_avatar( get_the_author_meta( 'ID' ) ); ?>
                                                                </header>
                                                                <div class="c-card__body flex-item--bottom">
                                                                    <header class="c-card__header u-pillar-box--none">
                                                                        <h6 class="c-heading h2 h-inside-black"><?php the_author_posts_link(); ?></h6>
                                                                    </header>
                                                                    <div class="author-icons">
                                                                        <?php
                      $twitter_profile = get_the_author_meta( 'twitter_profile' );
                      if ( $twitter_profile && $twitter_profile != '' ) {
                        echo '<a href="' . esc_url($twitter_profile) . '" rel="author" target="blank" class="h6 h-black-link">Twitter</a>';
                      }
                      $facebook_profile = get_the_author_meta( 'facebook_profile' );
                        if ( $facebook_profile && $facebook_profile != '' ) {
                          echo '<a href="' . esc_url($facebook_profile) . '" rel="author" target="blank" class="h6 h-black-link">Facebook</a>';
                        }
                      $instagram_profile = get_the_author_meta( 'instagram_profile' );
                        if ( $instagram_profile && $instagram_profile != '' ) {
                          echo '<a href="' . esc_url($instagram_profile) . '" rel="author" target="blank" class="h6 h-black-link">Instagram</a>';
                        }
                      $website_profile = get_the_author_meta( 'website_profile' );
                        if ( $website_profile && $website_profile != '' ) {
                          echo '<a href="' . esc_url($website_profile) . '" rel="author" target="blank" class="h6 h-black-link">Website</a>';
                        }
                    ?>
                                                                    </div>
                                                                    <p class="c-text"><?php the_author_meta( 'description' ); ?></p>
                                                                </div>
                                                                <footer class="c-card__footer c-card__footer--block flex-item--bottom">
</footer>
                                                            </div>
                                                            <!--Col 1-->
                                                        </div>
                                                        <!--Grid-->
                                                    </div>
                                                <?php endwhile; ?>
                                                <?php wp_reset_postdata(); ?>
                                            </div>
                                        <?php endif; ?>
                                        <!--Card-->
                                        <!--Card-->
                                    </div>
                                    <!--Container Card-->
                                    <!--Container Cards-->
                                    <?php
                                        $kcoreyall_args = array(
                                          'author_name' => 'Corey'
                                        )
                                    ?>
                                    <?php $kcoreyall = new WP_Query( $kcoreyall_args ); ?>
                                    <?php if ( $kcoreyall->have_posts() ) : ?>
                                        <div class="o-container o-container--large">
                                            <!--Grid-->
                                            <div class="o-grid o-grid--small-full o-grid--medium-full flex-wrap">
                                                <!--Col 1-->
                                                <?php while ( $kcoreyall->have_posts() ) : $kcoreyall->the_post(); ?>
                                                    <div class="o-grid__cell o-grid__cell--width-33@medium u-letter-box--medium card-hover">
                                                        <!--Card-->
                                                        <div class="c-card">
                                                            <div class="posts-thumb">
                                                                <a href="<?php echo esc_url( wp_get_shortlink()); ?>"></a>
                                                                <?php the_post_thumbnail( 'medium_large', array(
                                                                      'class' => 'lazy-load o-image'
                                                                ) ); ?>
                                                            </div>
                                                            <div class="u-window-box--small">
                                                                <header class="c-card__header flex-box-column posts-header">
                                                                    <h4 class="c-heading__sub u-centered c-uppercase h-inside-black"><?php the_category( ' | ' ); ?></h4>
                                                                    <a class="c-heading u-centered h2 h-link flex-item--bottom" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php the_title(); ?></a>
                                                                    <h6 class="c-heading__sub u-centered flex-item--bottom h-inside-grey"><?php the_author_posts_link(); ?></h6>
                                                                </header>
                                                                <div class="c-card__body u-centered posts-body">
                                                                    <?php echo get_excerpt(100); ?>
                                                                </div>
                                                                <footer class="c-card__footer c-card__footer--block u-centered">
                                                                    <a class="c-link c-uppercase" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php _e( 'Read More', 'cooper' ); ?></a>
                                                                </footer>
                                                            </div>
                                                            <!--Card Image-->
                                                            <!--Card Image-->
                                                            <!--Card Header-->
                                                            <!--Card Header-->
                                                            <!--Card Body-->
                                                            <!--Card Body-->
                                                            <!--Card Footer-->
                                                            <!--Card Footer-->
                                                        </div>
                                                        <!--Card-->
                                                    </div>
                                                <?php endwhile; ?>
                                                <?php wp_reset_postdata(); ?>
                                                <!--Col 1-->
                                                <!--Col 2-->
                                                <!--Col 2-->
                                                <!--Col 3-->
                                                <!--Col 3-->
                                            </div>
                                            <?php echo do_shortcode('[ajax_load_more container_type="div" css_classes="o-grid o-grid--small-full o-grid--medium-full flex-wrap" repeater="template_2" author="15" post_type="post" posts_per_page="6" post_format="standard" cache="true" offset="6" pause="true" scroll="false" transition_container="false" images_loaded="true"]'); ?>
                                            <!--Grid-->
                                        </div>
                                    <?php endif; ?>
                                    <!--Container Cards-->
                                </div>
                                <!--Tab Content 6-->
                                <!--Tab Content-->
                            </div>
                            <!--Tab Headings-->
                        </div>
                    </div>
                    <!--Container-->
                </div>
                <!--Full Container-->
            </section>
            <!--Tabs-->
            <!--Instagram-->
            <section>
</section>
            <!--Instagram-->
            <!--Social-->
            <!--Social-->            

<?php get_footer(); ?>