<!doctype html>
<html <?php language_attributes(); ?>>
    <?php if (!empty($related_posts)) { ?>
        <!-- <section class="u-bg-grey-light post-related"> -->
        <!-- <div class="o-container o-container--large u-letter-box--large"> -->
        <h5 class="u-centered"><?php _e( 'Related Posts', 'cooper' ); ?></h5>
        <!--Grid-->
        <div class="o-grid o-grid--small-full o-grid--medium-full flex-wrap">
            <?php
            foreach ($related_posts_theme as $post) {
              setup_postdata($post);
          ?>
                <!--Col 1-->
                <div class="o-grid__cell o-grid__cell--width-33@medium u-letter-box--medium card-hover">
                    <!--Card-->
                    <div class="c-card">
                        <div class="posts-thumb">
                            <a href="<?php echo esc_url( wp_get_shortlink()); ?>"></a>
                            <?php the_post_thumbnail( 'medium_large', array(
                                  'class' => 'lazy-load o-image'
                            ) ); ?>
                        </div>
                        <div class="u-window-box--small">
                            <header class="c-card__header flex-box-column posts-header">
                                <h4 class="c-heading__sub u-centered c-uppercase h-inside-black"><?php the_category( ' | ' ); ?></h4>
                                <a class="c-heading u-centered h2 h-link flex-item--bottom" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php the_title(); ?></a>
                                <h6 class="c-heading__sub u-centered flex-item--bottom h-inside-grey"><?php the_author_posts_link(); ?></h6>
                            </header>
                            <div class="c-card__body u-centered posts-body">
                                <?php echo get_excerpt(100); ?>
                            </div>
                            <footer class="c-card__footer c-card__footer--block u-centered">
                                <a class="c-link c-uppercase" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php _e( 'Read More', 'cooper' ); ?></a>
                            </footer>
                        </div>
                    </div>
                    <!--Card-->
                </div>
                <!--Col 1-->
            <?php } ?>
        </div>
        <!--Grid-->
        <!-- </section> -->
    <?php
    } ?>
</html>
