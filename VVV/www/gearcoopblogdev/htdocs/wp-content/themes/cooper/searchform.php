<!doctype html>
<html <?php language_attributes(); ?>>
    <form id="searchform" class="c-input-group search-form" method="get" action="<?php echo home_url('/'); ?>" data-anijs="if: mouseleave, do: fade-out animated, to: .search-form;if: mouseleave, do: input-search, to: .input-search">
        <div class="o-field">
            <input class="c-field input-search" type="text" name="s" placeholder="Search Posts" value="<?php the_search_query(); ?>">
        </div>
        <button class="c-button c-button--brand" type="submit" value="Search">
            <i class="fa fa-search" aria-hidden="true"></i>
        </button>
    </form>
</html>
