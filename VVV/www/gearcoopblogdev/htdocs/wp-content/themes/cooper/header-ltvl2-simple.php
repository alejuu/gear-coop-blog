<!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php wp_head(); ?>
        <?php wp_enqueue_style( 'ltvl', get_template_directory_uri() . '/css/ltvl2.css', false, null, 'all'); ?>
    </head>
    <body>
        <!--Main Content-->
        <main class="no-margin">
