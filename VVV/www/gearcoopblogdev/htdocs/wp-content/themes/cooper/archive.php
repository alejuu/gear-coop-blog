<?php
get_header(); ?>

            <section>
                <header class="u-centered u-bg-white">
                    <div class="u-window-box--large u-bg-white">
                        <h1><?php echo single_cat_title(); ?></h1>
                        <?php echo category_description(); ?>
                    </div>
                </header>
            </section>
            <section>
                <?php if ( have_posts() ) : ?>
                    <div class="o-container o-container--large u-letter-box--medium">
                        <!--Grid-->
                        <div class="o-grid o-grid--small-full o-grid--medium-full flex-wrap">
                            <!--Col 1-->
                            <?php while ( have_posts() ) : the_post(); ?>
                                <div class="o-grid__cell o-grid__cell--width-33@medium u-letter-box--medium card-hover">
                                    <!--Card-->
                                    <div class="c-card">
                                        <div class="posts-thumb">
                                            <a href="<?php echo esc_url( wp_get_shortlink()); ?>"></a>
                                            <?php the_post_thumbnail( 'medium_large', array(
                                                  'class' => 'lazy-load o-image'
                                            ) ); ?>
                                        </div>
                                        <div class="u-window-box--small">
                                            <header class="c-card__header flex-box-column posts-header">
                                                <h4 class="c-heading__sub u-centered c-uppercase h-inside-black"><?php the_category( ' | ' ); ?></h4>
                                                <a class="c-heading u-centered h2 h-link flex-item--bottom" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php the_title(); ?></a>
                                                <h6 class="c-heading__sub u-centered flex-item--bottom h-inside-grey"><?php the_author_posts_link(); ?></h6>
                                            </header>
                                            <div class="c-card__body u-centered posts-body">
                                                <?php echo get_excerpt(100); ?>
                                            </div>
                                            <footer class="c-card__footer c-card__footer--block u-centered">
                                                <a class="c-link c-uppercase" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php _e( 'Read More', 'cooper' ); ?></a>
                                            </footer>
                                        </div>
                                    </div>
                                    <!--Card-->
                                </div>
                            <?php endwhile; ?>
                            <!--Col 1-->
                        </div>
                        <?php
if(is_category()){
   $cat = get_query_var('cat');
   $category = get_category ($cat);
   echo do_shortcode('[ajax_load_more container_type="div" css_classes="o-grid o-grid--small-full o-grid--medium-full flex-wrap" repeater="template_2" post_type="post" posts_per_page="6" post_format="standard" category="'.$category->slug.'" cache="true" cache_id="cache-'.$category->slug.'" offset="6" pause="true" scroll="false" transition_container="false" images_loaded="true"]');
}
?>
                        <!--Grid-->
                    </div>
                <?php else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.', 'cooper' ); ?></p>
                <?php endif; ?>
            </section>            

<?php get_footer(); ?>