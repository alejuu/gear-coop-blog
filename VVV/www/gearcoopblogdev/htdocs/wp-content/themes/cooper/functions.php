<?php
if ( ! function_exists( 'cooper_setup' ) ) :

function cooper_setup() {

    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     */
    /* Pinegrow generated Load Text Domain Begin */
    load_theme_textdomain( 'cooper', get_template_directory() . '/languages' );
    /* Pinegrow generated Load Text Domain End */

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
     * Let WordPress manage the document title.
     */
    add_theme_support( 'title-tag' );

    /*
     * Enable support for Post Thumbnails on posts and pages.
     */
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 825, 510, true );

    // Add menus.
    register_nav_menus( array(
        'primary' => __( 'Primary Menu', 'cooper' ),
        'secondary' => __( 'Secondary Menu', 'cooper' ),
        'topbar'  => __( 'Top Bar', 'cooper' ),
        'mobile'  => __( 'Mobile Menu', 'cooper' ),
    ) );

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
        'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
    ) );

    /*
     * Enable support for Post Formats.
     */
    add_theme_support( 'post-formats', array(
        'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
    ) );
}
endif; // cooper_setup

add_action( 'after_setup_theme', 'cooper_setup' );


if ( ! function_exists( 'cooper_init' ) ) :

function cooper_init() {


    // Use categories and tags with attachments
    register_taxonomy_for_object_type( 'category', 'attachment' );
    register_taxonomy_for_object_type( 'post_tag', 'attachment' );

    /*
     * Register custom post types. You can also move this code to a plugin.
     */
    /* Pinegrow generated Custom Post Types Begin */

    /* Pinegrow generated Custom Post Types End */

    /*
     * Register custom taxonomies. You can also move this code to a plugin.
     */
    /* Pinegrow generated Taxonomies Begin */

    /* Pinegrow generated Taxonomies End */

}
endif; // cooper_setup

add_action( 'init', 'cooper_init' );


if ( ! function_exists( 'cooper_widgets_init' ) ) :

function cooper_widgets_init() {

    /*
     * Register widget areas.
     */
    /* Pinegrow generated Register Sidebars Begin */

    /* Pinegrow generated Register Sidebars End */
}
add_action( 'widgets_init', 'cooper_widgets_init' );
endif;// cooper_widgets_init



if ( ! function_exists( 'cooper_customize_register' ) ) :

function cooper_customize_register( $wp_customize ) {
    // Do stuff with $wp_customize, the WP_Customize_Manager object.

    /* Pinegrow generated Customizer Controls Begin */

    /* Pinegrow generated Customizer Controls End */

}
add_action( 'customize_register', 'cooper_customize_register' );
endif;// cooper_customize_register


if ( ! function_exists( 'cooper_enqueue_scripts' ) ) :
    function cooper_enqueue_scripts() {

        /* Pinegrow generated Enqueue Scripts Begin */

    wp_enqueue_script( 'customscripts', get_template_directory_uri() . '/js/scripts.min.js', array( 'jquery' ), null, true );

    /* Pinegrow generated Enqueue Scripts End */

        /* Pinegrow generated Enqueue Styles Begin */

    wp_deregister_style( 'fontawesome' );
    wp_enqueue_style( 'fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css', false, null, 'all');

    wp_deregister_style( 'style-1' );
    wp_enqueue_style( 'style-1', 'https://fonts.googleapis.com/css?family=Open+Sans:300,600,700|Roboto:700,900', false, null, 'all');

    wp_deregister_style( 'style' );
    wp_enqueue_style( 'style', get_bloginfo('stylesheet_url'), false, null, 'all');

    /* Pinegrow generated Enqueue Styles End */

    }
    add_action( 'wp_enqueue_scripts', 'cooper_enqueue_scripts' );
endif;

/*
 * Resource files included by Pinegrow.
 */
/* Pinegrow generated Include Resources Begin */
require_once "inc/wp_smart_navwalker.php";

    /* Pinegrow generated Include Resources End */
// Mobile navigation
require_once "inc/wp_smart_navwalker_mobile.php";
// Limit except length to 125 characters.
// tn limited excerpt length by number of characters
function get_excerpt( $count ) {
$excerpt = get_the_content();
$excerpt = strip_tags($excerpt);
$excerpt = substr($excerpt, 0, $count);
$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
$excerpt = '<p>'.$excerpt.'... </p>';
return $excerpt;
}
//Img posts

add_filter( 'the_content', 'wpse_add_img_post_class' );
function wpse_add_img_post_class( $content ) {
    // Bail if there is no content to work with.
    if ( ! $content ) {
        return $content;
    }

    // Create an instance of DOMDocument.
    $dom = new \DOMDocument();

    // Supress errors due to malformed HTML.
    // See http://stackoverflow.com/a/17559716/3059883
    $libxml_previous_state = libxml_use_internal_errors( true );

    // Populate $dom with $content, making sure to handle UTF-8.
    // Also, make sure that the doctype and HTML tags are not added to our
    // HTML fragment. http://stackoverflow.com/a/22490902/3059883
    $dom->loadHTML( mb_convert_encoding( $content, 'HTML-ENTITIES', 'UTF-8' ),
          LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD );

    // Restore previous state of libxml_use_internal_errors() now that we're done.
    libxml_use_internal_errors( $libxml_previous_state );

    // Create an instance of DOMXpath.
    $xpath = new \DOMXpath( $dom );

    // Get images then loop through and add additional classes.
    $imgs = $xpath->query( "//img" );
    foreach ( $imgs as $img ) {
        $existing_class = $img->getAttribute( 'class' );
        $img->setAttribute( 'class', "{$existing_class} post-image lazy-load o-image" );
    }

    // Save and return updated HTML.
    $new_content = $dom->saveHTML();
    return $new_content;
}

/**
 * Lazyload Converter - Sets up images for  lazy loading:
 *  - Affects only images with the .lazy-load class.
 *  - Assigns original src value to data-src attribute
 *  - Replaces original src value with a placeholder image
 *  - Assigns original srcset value to data-srcset
 *  - Replaces original srcset value to empty string
 *  - Creates noscript tag containing fallback HTML
 *
 * @param string $content
 * @return string
 */
add_filter( 'the_content', 'wpse_add_lazyload' );
add_filter( 'post_thumbnail_html', 'wpse_add_lazyload' );
function wpse_add_lazyload( $content ) {
    $placeholder = get_template_directory_uri() . '/assets/placeholders/squares.svg';

    // Create an instance of DOMDocument.
    $dom = new \DOMDocument();

    // Supress errors due to malformed HTML.
    // See http://stackoverflow.com/a/17559716/3059883
    $libxml_previous_state = libxml_use_internal_errors( true );

    // Populate $dom with $content, making sure to handle UTF-8, otherwise
    // problems will occur with UTF-8 characters.
    // Also, make sure that the doctype and HTML tags are not added to our HTML fragment. http://stackoverflow.com/a/22490902/3059883
    $dom->loadHTML( mb_convert_encoding( $content, 'HTML-ENTITIES', 'UTF-8' ), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD );

    // Restore previous state of libxml_use_internal_errors() now that we're done.
    libxml_use_internal_errors( $libxml_previous_state );

    // Create an instance of DOMXpath.
    $xpath = new \DOMXpath( $dom );

    // Match elements with the lazy-load class (note space around class name)
    // See http://stackoverflow.com/a/26126336/3059883
    $lazy_load_images = $xpath->query( "//img[ contains( concat( ' ', normalize-space( @class ), ' '), ' lazy-load ' ) ]" );

    // Process image HTML
    foreach ( $lazy_load_images as $node ) {
        $fallback = $node->cloneNode( true );

        $oldsrc = $node->getAttribute( 'src' );
        $node->setAttribute( 'data-src', $oldsrc );
        $newsrc = $placeholder;
        $node->setAttribute( 'src', $newsrc );

        $oldsrcset = $node->getAttribute( 'srcset' );
        $node->setAttribute( 'data-srcset', $oldsrcset );
        $newsrcset = '';
        $node->setAttribute( 'srcset', $newsrcset );

        $noscript = $dom->createElement( 'noscript', '' );
        $node->parentNode->insertBefore( $noscript, $node );
        $noscript->appendChild( $fallback );
    }

    // Save and return updated HTML.
    $new_content = $dom->saveHTML();
    return  $new_content;
}
/* Author Social Media Icons */
function add_to_author_profile( $contactmethods ) {
    $contactmethods['rss_url'] = 'RSS URL';
    $contactmethods['twitter_profile'] = 'Twitter Profile URL';
    $contactmethods['facebook_profile'] = 'Facebook Profile URL';
    $contactmethods['instagram_profile'] = 'Instagram Profile URL';
    $contactmethods['website_profile'] = 'Website Profile URL';
    return $contactmethods;
}
add_filter( 'user_contactmethods', 'add_to_author_profile', 10, 1);
/* Social sharing */
function coopershare_social_sharing_buttons($content) {
	 global $post;
	  if(is_singular() || is_home()){

		    // Get current page URL
		    $coopershareURL = urlencode(get_permalink());

		    // Get current page title
		    $coopershareTitle = str_replace( ' ', '%20', get_the_title());

    		// Get Post Thumbnail for pinterest
    		$coopershareThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );

    		// Construct sharing URL without using any script
    		$twitterURL = 'https://twitter.com/intent/tweet?text='.$coopershareTitle.'&amp;url='.$coopershareURL.'&amp;via=gearcoop';
    		$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$coopershareURL;
    		$googleURL = 'https://plus.google.com/share?url='.$coopershareURL;

    		// Based on popular demand added Pinterest too
    		$pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$coopershareURL.'&amp;media='.$coopershareThumbnail[0].'&amp;description='.$coopershareTitle;

        // Add sharing button at the end of page/page content
    		$variable .= '<div class="coopershare-social">';
    		$variable .= '<a class="coopershare-link coopershare-twitter c-link--white" href="'. $twitterURL .'" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>';
    		$variable .= '<a class="coopershare-link coopershare-facebook c-link--white" href="'.$facebookURL.'" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>';
    		$variable .= '<a class="coopershare-link coopershare-googleplus c-link--white" href="'.$googleURL.'" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a>';
    		$variable .= '<a class="coopershare-link coopershare-pinterest c-link--white" href="'.$pinterestURL.'" data-pin-custom="true" target="_blank"><i class="fa fa-pinterest" aria-hidden="true"></i></a>';
    		$variable .= '</div>';

        return $variable.$content;
    }else{
  		// if not a post/page then don't include sharing button
  	return $variable.$content;
    }
};
add_filter( 'the_content', 'coopershare_social_sharing_buttons');
/**
 * Related posts
 *
 * @global object $post
 * @param array $args
 * @return
 */
function wcr_related_posts($args = array()) {
    global $post;

    // default args
    $args = wp_parse_args($args, array(
        'post_id' => !empty($post) ? $post->ID : '',
        'taxonomy' => 'category',
        'limit' => 3,
        'post_type' => !empty($post) ? $post->post_type : 'post',
        'orderby' => 'date',
        'order' => 'DESC'
    ));

    // check taxonomy
    $registered_taxonomies = get_taxonomies();

    if (!in_array($args['taxonomy'], $registered_taxonomies)) {
        return;
    }

    // post taxonomies
    $taxonomies = wp_get_post_terms($args['post_id'], $args['taxonomy'], array('fields' => 'ids'));

    if (empty($taxonomies)) {
        return;
    }

    // query
    $related_posts_theme = get_posts(array(
        'post__not_in' => (array) $args['post_id'],
        'post_type' => $args['post_type'],
        'tax_query' => array(
            array(
                'taxonomy' => $args['taxonomy'],
                'field' => 'term_id',
                'terms' => $taxonomies
            ),
        ),
        // 'posts_per_page' => $args['limit'],
        'posts_per_page' => 3,
        'orderby' => $args['orderby'],
        'order' => $args['order']
    ));

    // query ajax_load_more
    $related_posts = get_posts(array(
      'post__not_in' => (array) $args['post_id'],
      'post_type' => $args['post_type'],
      'tax_query' => array(
          array(
              'taxonomy' => $args['taxonomy'],
              'field' => 'term_id',
              'terms' => $taxonomies
          ),
      ),
      // 'posts_per_page' => $args['limit'],
      'posts_per_page' => 12,
      'orderby' => $args['orderby'],
      'order' => $args['order']
    ));

    $post_in = array();
    foreach ( $related_posts_theme as $post ){
       setup_postdata( $post );
       $post_in[] = $post->ID; // Store post IDs
    }

    $post_in = array();
    foreach ( $related_posts as $post ){
       setup_postdata( $post );
       $post_in[] = $post->ID; // Store post IDs
    }

    include( locate_template('related-posts.php', false, false) );

    wp_reset_postdata();

    echo do_shortcode('[ajax_load_more ajax_load_more container_type="div" css_classes="o-grid o-grid--small-full o-grid--medium-full flex-wrap" repeater="template_2" post_type="post" posts_per_page="3" post_format="standard" post__in="'.implode(',', $post_in).'" cache="true" offset="3" pause="true" scroll="false" transition_container="false" images_loaded="true"]');
};
/**
 * Give Form Require Fields
 *
 * @description: Cusomize the prefix of this function before using. The $required_fields array key is the name of the field you would like to make required; ie " name='card_address_2' " - find using Debug Tools, etc.
 *
 * @param $required_fields
 *
 * @return mixed
 */
function mycustomprefix_give_form_required_fields( $required_fields ) {

	//Last Name
    $required_fields['give_last'] = array(
        'error_id' => 'invalid_last_name',
        'error_message' => __( 'Please enter your last name.', 'give' )
    );

    return $required_fields;
}
add_filter( 'give_purchase_form_required_fields', 'mycustomprefix_give_form_required_fields' );
?>
