<?php
get_header(); ?>

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <section class="post-header">
            <header class="u-centered u-bg-white">
                <div class="o-container featured-image">
                    <div class="o-container o-container--medium u-window-box--large u-absolute-center post-heading">
                        <h1 class="u-color-white"><?php the_title(); ?></h1>
                    </div>
                    <div class="featured-image-container u-absolute-center">
                        <?php the_post_thumbnail( null, array(
                              'class' => 'lazy-load o-image featured-image-img u-absolute-center'
                        ) ); ?>
                    </div>
                </div>
            </header>
        </section>
    <?php endwhile; ?>
<?php endif; ?>
<section class="u-bg-white post-content">
    <div class="o-container o-container--large u-window-box--large">
        <div class="u-window-box--medium">
            <?php the_content(); ?>
        </div>
    </div>
</section>            

<?php get_footer(); ?>