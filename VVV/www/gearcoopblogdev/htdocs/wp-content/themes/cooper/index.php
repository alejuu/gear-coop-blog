<?php
get_header(); ?>

            <!--Main Slider-->
            <section>
                <?php
                    $featuredhome_args = array(
                      'category_name' => 'featured-story'
                    )
                ?>
                <?php $featuredhome = new WP_Query( $featuredhome_args ); ?>
                <?php if ( $featuredhome->have_posts() ) : ?>
                    <div class="carousel">
                        <div class="carousel-inner">
                            <!--Carousel Controllers-->
                            <input class="carousel-open carousel-1" type="radio" id="carousel-1" name="carousel" aria-hidden="true" hidden="" checked="checked">
                            <input class="carousel-open carousel-2" type="radio" id="carousel-2" name="carousel" aria-hidden="true" hidden="">
                            <input class="carousel-open carousel-3" type="radio" id="carousel-3" name="carousel" aria-hidden="true" hidden="">
                            <!--Carousel Controllers-->
                            <!--Carousel Item 1-->
                            <?php while ( $featuredhome->have_posts() ) : $featuredhome->the_post(); ?>
                                <div class="carousel-item">
                                    <div class="carousel-content flex-box-column u-window-box--xlarge">
                                        <a href="<?php echo esc_url( wp_get_shortlink()); ?>"></a>
                                        <!--Carousel Item 1 Preheader-->
                                        <div class="carousel-preheader u-window-box--small hide-responsive hide-xsmall">
                                            <h3><?php _e( 'Featured', 'cooper' ); ?></h3>
                                        </div>
                                        <!--Carousel Item 1 Preheader-->
                                        <!--Carousel Item 1 Header-->
                                        <div class="carousel-header u-window-box--small flex-item--bottom">
                                            <a href="<?php echo esc_url( wp_get_shortlink()); ?>"></a>
                                            <h4 class="c-heading__sub h-inside-white hide-responsive hide-small"><?php the_category( ' | ' ); ?></h4>
                                            <a class="h-white-link h1" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php the_title(); ?></a>
                                            <h6 class="hide-responsive hide-small"><?php the_author(); ?></h6>
                                        </div>
                                        <!--Carousel Item 1 Header-->
                                    </div>
                                    <!--Carousel Item 1 Image-->
                                    <?php the_post_thumbnail( 'full', array(
                                          'class' => 'lazy-load'
                                    ) ); ?>
                                    <!--Carousel Item 1 Image-->
                                </div>
                            <?php endwhile; ?>
                            <?php wp_reset_postdata(); ?>
                            <!--Carousel Item 1-->
                            <!--Carousel Item 2-->
                            <!--Carousel Item 2-->
                            <!--Carousel Item 3-->
                            <!--Carousel Item 3-->
                            <!--Carousel Controllers-->
                            <label for="carousel-3" class="carousel-control prev control-1" data-anijs="if: click, do: fade-in animated, to: .carousel-item">
                                <?php _e( '‹', 'cooper' ); ?>
                            </label>
                            <label for="carousel-2" class="carousel-control next control-1" data-anijs="if: click, do: fade-in animated, to: .carousel-item">
                                <?php _e( '›', 'cooper' ); ?>
                            </label>
                            <label for="carousel-1" class="carousel-control prev control-2" data-anijs="if: click, do: fade-in animated, to: .carousel-item">
                                <?php _e( '‹', 'cooper' ); ?>
                            </label>
                            <label for="carousel-3" class="carousel-control next control-2" data-anijs="if: click, do: fade-in animated, to: .carousel-item">
                                <?php _e( '›', 'cooper' ); ?>
                            </label>
                            <label for="carousel-2" class="carousel-control prev control-3" data-anijs="if: click, do: fade-in animated, to: .carousel-item">
                                <?php _e( '‹', 'cooper' ); ?>
                            </label>
                            <label for="carousel-1" class="carousel-control next control-3" data-anijs="if: click, do: fade-in animated, to: .carousel-item">
                                <?php _e( '›', 'cooper' ); ?>
                            </label>
                            <!--Carousel Controllers-->
                            <!--Carousel Bullets-->
                            <ol class="carousel-indicators">
                                <li>
                                    <label for="carousel-1" class="carousel-bullet" data-anijs="if: click, do: fade-in animated, to: .carousel-item">
                                        <?php _e( '○', 'cooper' ); ?>
                                    </label>
                                </li>
                                <li>
                                    <label for="carousel-2" class="carousel-bullet" data-anijs="if: click, do: fade-in animated, to: .carousel-item">
                                        <?php _e( '○', 'cooper' ); ?>
                                    </label>
                                </li>
                                <li>
                                    <label for="carousel-3" class="carousel-bullet" data-anijs="if: click, do: fade-in animated, to: .carousel-item">
                                        <?php _e( '○', 'cooper' ); ?>
                                    </label>
                                </li>
                            </ol>
                            <!--Carousel Bullets-->
                        </div>
                    </div>
                <?php else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.', 'cooper' ); ?></p>
                <?php endif; ?>
            </section>
            <!--Main Slider-->
            <!--Tabs-->
            <section>
                <!--Full Container-->
                <div class="o-container">
                    <!--Tabs BG-->
                    <div class="u-bg-white w-full tabs-menu">
</div>
                    <!--Tabs BG-->
                    <!--Container-->
                    <div class="o-container o-container--large home-tabs">
                        <div class="c-tabs u-centered">
                            <!--Tab Headings-->
                            <div class="c-tabs__headings tab-frame flex-wrap">
                                <input type="radio" checked name="tab" id="home-tab--1">
                                <label class="c-tab-heading h3" for="home-tab--1" data-anijs="if: click, do: fade-in animated, to: #tab-1--content">
                                    <?php _e( 'All', 'cooper' ); ?>
                                </label>
                                <input type="radio" name="tab" id="home-tab--2">
                                <label class="c-tab-heading h3" for="home-tab--2" data-anijs="if: click, do: fade-in animated, to: #tab-2--content">
                                    <?php _e( 'Reviews', 'cooper' ); ?>
                                </label>
                                <input type="radio" name="tab" id="home-tab--3">
                                <label class="c-tab-heading h3" for="home-tab--3" data-anijs="if: click, do: fade-in animated, to: #tab-3--content">
                                    <?php _e( 'Guides', 'cooper' ); ?>
                                </label>
                                <input type="radio" name="tab" id="home-tab--4">
                                <label class="c-tab-heading h3" for="home-tab--4" data-anijs="if: click, do: fade-in animated, to: #tab-4--content">
                                    <?php _e( 'Journals', 'cooper' ); ?>
                                </label>
                                <!--Tab Content 1-->
                                <div id="tab-1--content" class="c-tabs__tab tab-content">
                                    <!--Container Card-->
                                    <div class="o-container o-container--xlarge u-window-box--medium card-hover">
                                        <?php
                                            $featuredall_args = array(
                                              'post_type' => 'post'
                                            )
                                        ?>
                                        <?php $featuredall = new WP_Query( $featuredall_args ); ?>
                                        <?php if ( $featuredall->have_posts() ) : ?>
                                            <div class="c-card w-full">
                                                <?php $featuredall_item_number = 0; ?>
                                                <?php while ( $featuredall->have_posts() && $featuredall_item_number++ < 1 ) : $featuredall->the_post(); ?>
                                                    <div class="c-card__item u-window-box--none">
                                                        <!--Grid-->
                                                        <div class="o-grid o-grid--small-full o-grid--medium-full">
                                                            <!--Col 1-->
                                                            <div class="o-grid__cell o-grid__cell--featured-1 u-window-box--none">
                                                                <div class="posts-featured">
                                                                    <a href="<?php echo esc_url( wp_get_shortlink()); ?>"></a>
                                                                    <?php the_post_thumbnail( 'medium_large', array(
                                                                          'class' => 'lazy-load o-image'
                                                                    ) ); ?>
                                                                    <!--Featured Img-->
                                                                    <!--Featured Img-->
                                                                </div>
                                                            </div>
                                                            <!--Col 1-->
                                                            <!--Col 2-->
                                                            <div class="o-grid__cell o-grid__cell--featured-2 flex-box-column u-left u-letter-box--medium">
                                                                <header class="c-card__header">
                                                                    <h3 class="c-heading__sub c-uppercase"><?php _e( 'Featured', 'cooper' ); ?></h3>
                                                                </header>
                                                                <div class="c-card__body flex-item--bottom">
                                                                    <header class="c-card__header u-pillar-box--none">
                                                                        <h4 class="c-heading__sub c-uppercase h-inside-black"><?php the_category( ' | ' ); ?></h4>
                                                                        <a class="c-heading h2 h-link" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php the_title(); ?></a>
                                                                        <h6 class="c-heading__sub h-inside-grey"><?php the_author_posts_link(); ?></h6>
                                                                    </header>
                                                                    <?php echo get_excerpt(150); ?>
                                                                </div>
                                                                <footer class="c-card__footer c-card__footer--block flex-item--bottom">
                                                                    <a class="c-link" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php _e( 'Read More', 'cooper' ); ?></a>
                                                                </footer>
                                                            </div>
                                                            <!--Col 2-->
                                                        </div>
                                                        <!--Grid-->
                                                    </div>
                                                <?php endwhile; ?>
                                                <?php wp_reset_postdata(); ?>
                                            </div>
                                        <?php endif; ?>
                                        <!--Card-->
                                        <!--Card-->
                                    </div>
                                    <!--Container Card-->
                                    <!--Container Cards-->
                                    <?php
                                        $all_args = array(
                                          'post_type' => 'post',
                                          'offset' => '1'
                                        )
                                    ?>
                                    <?php $all = new WP_Query( $all_args ); ?>
                                    <?php if ( $all->have_posts() ) : ?>
                                        <div class="o-container o-container--large">
                                            <!--Grid-->
                                            <div class="o-grid o-grid--small-full o-grid--medium-full flex-wrap">
                                                <!--Col 1-->
                                                <?php while ( $all->have_posts() ) : $all->the_post(); ?>
                                                    <div class="o-grid__cell o-grid__cell--width-33@medium u-letter-box--medium card-hover">
                                                        <!--Card-->
                                                        <div class="c-card">
                                                            <div class="posts-thumb">
                                                                <a href="<?php echo esc_url( wp_get_shortlink()); ?>"></a>
                                                                <?php the_post_thumbnail( 'medium_large', array(
                                                                      'class' => 'lazy-load o-image'
                                                                ) ); ?>
                                                            </div>
                                                            <div class="u-window-box--small">
                                                                <header class="c-card__header flex-box-column posts-header">
                                                                    <h4 class="c-heading__sub u-centered c-uppercase h-inside-black"><?php the_category( ' | ' ); ?></h4>
                                                                    <a class="c-heading u-centered h2 h-link flex-item--bottom" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php the_title(); ?></a>
                                                                    <h6 class="c-heading__sub u-centered flex-item--bottom h-inside-grey"><?php the_author_posts_link(); ?></h6>
                                                                </header>
                                                                <div class="c-card__body u-centered posts-body">
                                                                    <?php echo get_excerpt(100); ?>
                                                                </div>
                                                                <footer class="c-card__footer c-card__footer--block u-centered">
                                                                    <a class="c-link c-uppercase" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php _e( 'Read More', 'cooper' ); ?></a>
                                                                </footer>
                                                            </div>
                                                            <!--Card Image-->
                                                            <!--Card Image-->
                                                            <!--Card Header-->
                                                            <!--Card Header-->
                                                            <!--Card Body-->
                                                            <!--Card Body-->
                                                            <!--Card Footer-->
                                                            <!--Card Footer-->
                                                        </div>
                                                        <!--Card-->
                                                    </div>
                                                <?php endwhile; ?>
                                                <?php wp_reset_postdata(); ?>
                                                <!--Col 1-->
                                                <!--Col 2-->
                                                <!--Col 2-->
                                                <!--Col 3-->
                                                <!--Col 3-->
                                            </div>
                                            <?php echo do_shortcode('[ajax_load_more container_type="div" css_classes="o-grid o-grid--small-full o-grid--medium-full flex-wrap" repeater="template_2" post_type="post" posts_per_page="6" post_format="standard" cache="true" offset="7" pause="true" scroll="false" transition_container="false" images_loaded="true"]'); ?>
                                            <!--Grid-->
                                        </div>
                                    <?php endif; ?>
                                    <!--Container Cards-->
                                </div>
                                <!--Tab Content 1-->
                                <!--Tab Content 2-->
                                <div id="tab-2--content" class="c-tabs__tab tab-content">
                                    <!--Container Card-->
                                    <div class="o-container o-container--xlarge u-window-box--medium card-hover">
                                        <?php
                                            $reviews_args = array(
                                              'category_name' => 'reviews'
                                            )
                                        ?>
                                        <?php $reviews = new WP_Query( $reviews_args ); ?>
                                        <?php if ( $reviews->have_posts() ) : ?>
                                            <div class="c-card w-full">
                                                <?php $reviews_item_number = 0; ?>
                                                <?php while ( $reviews->have_posts() && $reviews_item_number++ < 1 ) : $reviews->the_post(); ?>
                                                    <div class="c-card__item u-window-box--none">
                                                        <!--Grid-->
                                                        <div class="o-grid o-grid--small-full o-grid--medium-full">
                                                            <!--Col 1-->
                                                            <div class="o-grid__cell o-grid__cell--featured-1 u-window-box--none">
                                                                <div class="posts-featured">
                                                                    <a href="<?php echo esc_url( wp_get_shortlink()); ?>"></a>
                                                                    <?php the_post_thumbnail( 'medium_large', array(
                                                                          'class' => 'o-image'
                                                                    ) ); ?>
                                                                    <!--Featured Img-->
                                                                    <!--Featured Img-->
                                                                </div>
                                                            </div>
                                                            <!--Col 1-->
                                                            <!--Col 2-->
                                                            <div class="o-grid__cell o-grid__cell--featured-2 flex-box-column u-left u-letter-box--medium">
                                                                <header class="c-card__header">
                                                                    <h3 class="c-heading__sub c-uppercase"><?php _e( 'Featured', 'cooper' ); ?></h3>
                                                                </header>
                                                                <div class="c-card__body flex-item--bottom">
                                                                    <header class="c-card__header u-pillar-box--none">
                                                                        <h4 class="c-heading__sub c-uppercase h-inside-black"><?php the_category( ' | ' ); ?></h4>
                                                                        <a class="c-heading h2 h-link" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php the_title(); ?></a>
                                                                        <h6 class="c-heading__sub h-inside-grey"><?php the_author_posts_link(); ?></h6>
                                                                    </header>
                                                                    <?php echo get_excerpt(150); ?>
                                                                </div>
                                                                <footer class="c-card__footer c-card__footer--block flex-item--bottom">
                                                                    <a class="c-link" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php _e( 'Read More', 'cooper' ); ?></a>
                                                                </footer>
                                                            </div>
                                                            <!--Col 2-->
                                                        </div>
                                                        <!--Grid-->
                                                    </div>
                                                <?php endwhile; ?>
                                                <?php wp_reset_postdata(); ?>
                                            </div>
                                        <?php endif; ?>
                                        <!--Card-->
                                        <!--Card-->
                                    </div>
                                    <!--Container Card-->
                                    <!--Container Cards-->
                                    <?php
                                        $reviewsall_args = array(
                                          'category_name' => 'reviews',
                                          'offset' => '1'
                                        )
                                    ?>
                                    <?php $reviewsall = new WP_Query( $reviewsall_args ); ?>
                                    <?php if ( $reviewsall->have_posts() ) : ?>
                                        <div class="o-container o-container--large">
                                            <!--Grid-->
                                            <div class="o-grid o-grid--small-full o-grid--medium-full flex-wrap">
                                                <!--Col 1-->
                                                <?php while ( $reviewsall->have_posts() ) : $reviewsall->the_post(); ?>
                                                    <div class="o-grid__cell o-grid__cell--width-33@medium u-letter-box--medium card-hover">
                                                        <!--Card-->
                                                        <div class="c-card">
                                                            <div class="posts-thumb">
                                                                <a href="<?php echo esc_url( wp_get_shortlink()); ?>"></a>
                                                                <?php the_post_thumbnail( 'medium_large', array(
                                                                      'class' => 'lazy-load o-image'
                                                                ) ); ?>
                                                            </div>
                                                            <div class="u-window-box--small">
                                                                <header class="c-card__header flex-box-column posts-header">
                                                                    <h4 class="c-heading__sub u-centered c-uppercase h-inside-black"><?php the_category( ' | ' ); ?></h4>
                                                                    <a class="c-heading u-centered h2 h-link flex-item--bottom" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php the_title(); ?></a>
                                                                    <h6 class="c-heading__sub u-centered flex-item--bottom h-inside-grey"><?php the_author_posts_link(); ?></h6>
                                                                </header>
                                                                <div class="c-card__body u-centered posts-body">
                                                                    <?php echo get_excerpt(100); ?>
                                                                </div>
                                                                <footer class="c-card__footer c-card__footer--block u-centered">
                                                                    <a class="c-link c-uppercase" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php _e( 'Read More', 'cooper' ); ?></a>
                                                                </footer>
                                                            </div>
                                                            <!--Card Image-->
                                                            <!--Card Image-->
                                                            <!--Card Header-->
                                                            <!--Card Header-->
                                                            <!--Card Body-->
                                                            <!--Card Body-->
                                                            <!--Card Footer-->
                                                            <!--Card Footer-->
                                                        </div>
                                                        <!--Card-->
                                                    </div>
                                                <?php endwhile; ?>
                                                <?php wp_reset_postdata(); ?>
                                                <!--Col 1-->
                                                <!--Col 2-->
                                                <!--Col 2-->
                                                <!--Col 3-->
                                                <!--Col 3-->
                                            </div>
                                            <?php echo do_shortcode('[ajax_load_more container_type="div" css_classes="o-grid o-grid--small-full o-grid--medium-full flex-wrap" repeater="template_2" category="reviews" post_type="post" posts_per_page="6" post_format="standard" cache="true" offset="7" pause="true" scroll="false" transition_container="false" images_loaded="true"]'); ?>
                                            <!--Grid-->
                                        </div>
                                    <?php endif; ?>
                                    <!--Container Cards-->
                                </div>
                                <!--Tab Content 2-->
                                <!--Tab Content 3-->
                                <div id="tab-3--content" class="c-tabs__tab tab-content">
                                    <!--Container Card-->
                                    <div class="o-container o-container--xlarge u-window-box--medium card-hover">
                                        <?php
                                            $guides_args = array(
                                              'category_name' => 'guides'
                                            )
                                        ?>
                                        <?php $guides = new WP_Query( $guides_args ); ?>
                                        <?php if ( $guides->have_posts() ) : ?>
                                            <div class="c-card w-full">
                                                <?php $guides_item_number = 0; ?>
                                                <?php while ( $guides->have_posts() && $guides_item_number++ < 1 ) : $guides->the_post(); ?>
                                                    <div class="c-card__item u-window-box--none">
                                                        <!--Grid-->
                                                        <div class="o-grid o-grid--small-full o-grid--medium-full">
                                                            <!--Col 1-->
                                                            <div class="o-grid__cell o-grid__cell--featured-1 u-window-box--none">
                                                                <div class="posts-featured">
                                                                    <a href="<?php echo esc_url( wp_get_shortlink()); ?>"></a>
                                                                    <?php the_post_thumbnail( 'medium_large', array(
                                                                          'class' => 'o-image'
                                                                    ) ); ?>
                                                                    <!--Featured Img-->
                                                                    <!--Featured Img-->
                                                                </div>
                                                            </div>
                                                            <!--Col 1-->
                                                            <!--Col 2-->
                                                            <div class="o-grid__cell o-grid__cell--featured-2 flex-box-column u-left u-letter-box--medium">
                                                                <header class="c-card__header">
                                                                    <h3 class="c-heading__sub c-uppercase"><?php _e( 'Featured', 'cooper' ); ?></h3>
                                                                </header>
                                                                <div class="c-card__body flex-item--bottom">
                                                                    <header class="c-card__header u-pillar-box--none">
                                                                        <h4 class="c-heading__sub c-uppercase h-inside-black"><?php the_category( ' | ' ); ?></h4>
                                                                        <a class="c-heading h2 h-link" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php the_title(); ?></a>
                                                                        <h6 class="c-heading__sub h-inside-grey"><?php the_author_posts_link(); ?></h6>
                                                                    </header>
                                                                    <?php echo get_excerpt(150); ?>
                                                                </div>
                                                                <footer class="c-card__footer c-card__footer--block flex-item--bottom">
                                                                    <a class="c-link" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php _e( 'Read More', 'cooper' ); ?></a>
                                                                </footer>
                                                            </div>
                                                            <!--Col 2-->
                                                        </div>
                                                        <!--Grid-->
                                                    </div>
                                                <?php endwhile; ?>
                                                <?php wp_reset_postdata(); ?>
                                            </div>
                                        <?php endif; ?>
                                        <!--Card-->
                                        <!--Card-->
                                    </div>
                                    <!--Container Card-->
                                    <!--Container Cards-->
                                    <?php
                                        $guidesall_args = array(
                                          'category_name' => 'guides',
                                          'offset' => '1'
                                        )
                                    ?>
                                    <?php $guidesall = new WP_Query( $guidesall_args ); ?>
                                    <?php if ( $guidesall->have_posts() ) : ?>
                                        <div class="o-container o-container--large">
                                            <!--Grid-->
                                            <div class="o-grid o-grid--small-full o-grid--medium-full flex-wrap">
                                                <!--Col 1-->
                                                <?php while ( $guidesall->have_posts() ) : $guidesall->the_post(); ?>
                                                    <div class="o-grid__cell o-grid__cell--width-33@medium u-letter-box--medium card-hover">
                                                        <!--Card-->
                                                        <div class="c-card">
                                                            <div class="posts-thumb">
                                                                <a href="<?php echo esc_url( wp_get_shortlink()); ?>"></a>
                                                                <?php the_post_thumbnail( 'medium_large', array(
                                                                      'class' => 'lazy-load o-image'
                                                                ) ); ?>
                                                            </div>
                                                            <div class="u-window-box--small">
                                                                <header class="c-card__header flex-box-column posts-header">
                                                                    <h4 class="c-heading__sub u-centered c-uppercase h-inside-black"><?php the_category( ' | ' ); ?></h4>
                                                                    <a class="c-heading u-centered h2 h-link flex-item--bottom" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php the_title(); ?></a>
                                                                    <h6 class="c-heading__sub u-centered flex-item--bottom h-inside-grey"><?php the_author_posts_link(); ?></h6>
                                                                </header>
                                                                <div class="c-card__body u-centered posts-body">
                                                                    <?php echo get_excerpt(100); ?>
                                                                </div>
                                                                <footer class="c-card__footer c-card__footer--block u-centered">
                                                                    <a class="c-link c-uppercase" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php _e( 'Read More', 'cooper' ); ?></a>
                                                                </footer>
                                                            </div>
                                                            <!--Card Image-->
                                                            <!--Card Image-->
                                                            <!--Card Header-->
                                                            <!--Card Header-->
                                                            <!--Card Body-->
                                                            <!--Card Body-->
                                                            <!--Card Footer-->
                                                            <!--Card Footer-->
                                                        </div>
                                                        <!--Card-->
                                                    </div>
                                                <?php endwhile; ?>
                                                <?php wp_reset_postdata(); ?>
                                                <!--Col 1-->
                                                <!--Col 2-->
                                                <!--Col 2-->
                                                <!--Col 3-->
                                                <!--Col 3-->
                                            </div>
                                            <?php echo do_shortcode('[ajax_load_more container_type="div" css_classes="o-grid o-grid--small-full o-grid--medium-full flex-wrap" repeater="template_2" post_type="post" category="guides" posts_per_page="6" post_format="standard" cache="true" offset="7" pause="true" scroll="false" transition_container="false" images_loaded="true"]'); ?>
                                            <!--Grid-->
                                        </div>
                                    <?php endif; ?>
                                    <!--Container Cards-->
                                </div>
                                <!--Tab Content 3-->
                                <!--Tab Content 4-->
                                <div id="tab-4--content" class="c-tabs__tab tab-content">
                                    <!--Container Card-->
                                    <div class="o-container o-container--xlarge u-window-box--medium card-hover">
                                        <?php
                                            $journals_args = array(
                                              'category_name' => 'journals'
                                            )
                                        ?>
                                        <?php $journals = new WP_Query( $journals_args ); ?>
                                        <?php if ( $journals->have_posts() ) : ?>
                                            <div class="c-card w-full">
                                                <?php $journals_item_number = 0; ?>
                                                <?php while ( $journals->have_posts() && $journals_item_number++ < 1 ) : $journals->the_post(); ?>
                                                    <div class="c-card__item u-window-box--none">
                                                        <!--Grid-->
                                                        <div class="o-grid o-grid--small-full o-grid--medium-full">
                                                            <!--Col 1-->
                                                            <div class="o-grid__cell o-grid__cell--featured-1 u-window-box--none">
                                                                <div class="posts-featured">
                                                                    <a href="<?php echo esc_url( wp_get_shortlink()); ?>"></a>
                                                                    <?php the_post_thumbnail( 'medium_large', array(
                                                                          'class' => 'o-image'
                                                                    ) ); ?>
                                                                    <!--Featured Img-->
                                                                    <!--Featured Img-->
                                                                </div>
                                                            </div>
                                                            <!--Col 1-->
                                                            <!--Col 2-->
                                                            <div class="o-grid__cell o-grid__cell--featured-2 flex-box-column u-left u-letter-box--medium">
                                                                <header class="c-card__header">
                                                                    <h3 class="c-heading__sub c-uppercase"><?php _e( 'Featured', 'cooper' ); ?></h3>
                                                                </header>
                                                                <div class="c-card__body flex-item--bottom">
                                                                    <header class="c-card__header u-pillar-box--none">
                                                                        <h4 class="c-heading__sub c-uppercase h-inside-black"><?php the_category( ' | ' ); ?></h4>
                                                                        <a class="c-heading h2 h-link" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php the_title(); ?></a>
                                                                        <h6 class="c-heading__sub h-inside-grey"><?php the_author_posts_link(); ?></h6>
                                                                    </header>
                                                                    <?php echo get_excerpt(150); ?>
                                                                </div>
                                                                <footer class="c-card__footer c-card__footer--block flex-item--bottom">
                                                                    <a class="c-link" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php _e( 'Read More', 'cooper' ); ?></a>
                                                                </footer>
                                                            </div>
                                                            <!--Col 2-->
                                                        </div>
                                                        <!--Grid-->
                                                    </div>
                                                <?php endwhile; ?>
                                                <?php wp_reset_postdata(); ?>
                                            </div>
                                        <?php endif; ?>
                                        <!--Card-->
                                        <!--Card-->
                                    </div>
                                    <!--Container Card-->
                                    <!--Container Cards-->
                                    <?php
                                        $journalsall_args = array(
                                          'category_name' => 'journals',
                                          'offset' => '1'
                                        )
                                    ?>
                                    <?php $journalsall = new WP_Query( $journalsall_args ); ?>
                                    <?php if ( $journalsall->have_posts() ) : ?>
                                        <div class="o-container o-container--large">
                                            <!--Grid-->
                                            <div class="o-grid o-grid--small-full o-grid--medium-full flex-wrap">
                                                <!--Col 1-->
                                                <?php while ( $journalsall->have_posts() ) : $journalsall->the_post(); ?>
                                                    <div class="o-grid__cell o-grid__cell--width-33@medium u-letter-box--medium card-hover">
                                                        <!--Card-->
                                                        <div class="c-card">
                                                            <div class="posts-thumb">
                                                                <a href="<?php echo esc_url( wp_get_shortlink()); ?>"></a>
                                                                <?php the_post_thumbnail( 'medium_large', array(
                                                                      'class' => 'lazy-load o-image'
                                                                ) ); ?>
                                                            </div>
                                                            <div class="u-window-box--small">
                                                                <header class="c-card__header flex-box-column posts-header">
                                                                    <h4 class="c-heading__sub u-centered c-uppercase h-inside-black"><?php the_category( ' | ' ); ?></h4>
                                                                    <a class="c-heading u-centered h2 h-link flex-item--bottom" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php the_title(); ?></a>
                                                                    <h6 class="c-heading__sub u-centered flex-item--bottom h-inside-grey"><?php the_author_posts_link(); ?></h6>
                                                                </header>
                                                                <div class="c-card__body u-centered posts-body">
                                                                    <?php echo get_excerpt(100); ?>
                                                                </div>
                                                                <footer class="c-card__footer c-card__footer--block u-centered">
                                                                    <a class="c-link c-uppercase" href="<?php echo esc_url( wp_get_shortlink()); ?>"><?php _e( 'Read More', 'cooper' ); ?></a>
                                                                </footer>
                                                            </div>
                                                            <!--Card Image-->
                                                            <!--Card Image-->
                                                            <!--Card Header-->
                                                            <!--Card Header-->
                                                            <!--Card Body-->
                                                            <!--Card Body-->
                                                            <!--Card Footer-->
                                                            <!--Card Footer-->
                                                        </div>
                                                        <!--Card-->
                                                    </div>
                                                <?php endwhile; ?>
                                                <?php wp_reset_postdata(); ?>
                                                <!--Col 1-->
                                                <!--Col 2-->
                                                <!--Col 2-->
                                                <!--Col 3-->
                                                <!--Col 3-->
                                            </div>
                                            <?php echo do_shortcode('[ajax_load_more container_type="div" css_classes="o-grid o-grid--small-full o-grid--medium-full flex-wrap" repeater="template_2" post_type="post" category="journals"  posts_per_page="6" post_format="standard" cache="true" offset="7" pause="true" scroll="false" transition_container="false" images_loaded="true"]'); ?>
                                            <!--Grid-->
                                        </div>
                                    <?php endif; ?>
                                    <!--Container Cards-->
                                </div>
                                <!--Tab Content 4-->
                                <!--Tab Content 5-->
                                <!--Tab Content 5-->
                                <!--Tab Content-->
                            </div>
                            <!--Tab Headings-->
                        </div>
                    </div>
                    <!--Container-->
                </div>
                <!--Full Container-->
            </section>
            <!--Tabs-->
            <!--Instagram-->
            <section>
</section>
            <!--Instagram-->
            <!--Social-->
            <section class="u-bg-white">
                <div class="o-container o-container--large u-window-box--xlarge">
                    <header class="u-centered">
                        <h2 class="c-light"><?php _e( '@GearCoop', 'cooper' ); ?></h2>
                    </header>
                    <div class="o-grid o-grid--medium-full flex-wrap u-letter-box--super u-centered">
                        <div class="o-grid__cell o-grid__cell--width-33 u-window-box--small u-centered">
                            <div class="c-avatar social-avatar u-centered-item flex-box--center">
                                <a href="https://www.instagram.com/gearcoop/" class="h-link"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div class="o-grid__cell o-grid__cell--width-33 u-window-box--small u-centered">
                            <div class="c-avatar social-avatar u-centered-item flex-box--center">
                                <a href="https://www.facebook.com/gearcoop" class="h-link"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div class="o-grid__cell o-grid__cell--width-33 u-window-box--small u-centered">
                            <div class="c-avatar social-avatar u-centered-item flex-box--center">
                                <a href="https://twitter.com/gearcoop" class="h-link"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--Social-->            

<?php get_footer(); ?>