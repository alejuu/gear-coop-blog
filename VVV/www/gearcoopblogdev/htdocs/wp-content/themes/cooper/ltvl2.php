<?php
/*
Template Name: Live That Van Life 2
*/
?>
<?php
get_header('ltvl2'); ?>

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <section class="post-header">
            <header class="u-centered u-bg-white">
                <div class="o-container featured-image">
                    <div class="o-container o-container--medium u-window-box--large u-absolute-center post-heading">
                        <h1 class="u-color-white"><?php the_title(); ?></h1>
                    </div>
                    <div class="featured-image-container u-absolute-center">
                        <?php the_post_thumbnail( null, array(
                              'class' => 'lazy-load o-image featured-image-img u-absolute-center'
                        ) ); ?>
                    </div>
                </div>
            </header>
        </section>
    <?php endwhile; ?>
<?php endif; ?>
<section class="u-bg-grey post-content">
    <div class="o-container o-container--large u-window-box--xlarge">
        <div class="u-window-box--medium u-centered">
          <img class="lazy-load o-image" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/placeholders/squares.svg" data-src="../wp-content/themes/gear-coop-blog/assets/images/LTVL_logo.png" />
        </div>
        <div class="u-window-box--medium u-centered">
          <h6 class="c-uppercase">We are no longer accepting applications.</h6>
          <h6 class="c-uppercase"><b>Follow our winners and find your inspiration.</b></h6>
          <a href="https://www.gearcoop.com/ambassadors/cade-and-becca/">Check Them Out</a>
        </div>
        <div class="u-window-box--medium ">
            <div class="o-grid flex-wrap flex-center">
                <div class="o-grid__cell o-grid__cell--width-33 u-letter-box--medium flex-box--center">
                    <img width="100" class="lazy-load o-image" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/placeholders/squares.svg" data-src="../wp-content/themes/gear-coop-blog/assets/images/tnf.png" />
                </div>
                <div class="o-grid__cell o-grid__cell--width-33 u-letter-box--medium flex-box--center">
                    <img width="100" class="lazy-load o-image" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/placeholders/squares.svg" data-src="../wp-content/themes/gear-coop-blog/assets/images/arcteryx.png" />
                </div>
                <div class="o-grid__cell o-grid__cell--width-33 u-letter-box--medium flex-box--center">
                    <img width="100" class="lazy-load o-image" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/placeholders/squares.svg" data-src="../wp-content/themes/gear-coop-blog/assets/images/lasportiva.png" />
                </div>
                <div class="o-grid__cell o-grid__cell--width-33 u-letter-box--medium flex-box--center">
                    <img width="100" class="lazy-load o-image" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/placeholders/squares.svg" data-src="../wp-content/themes/gear-coop-blog/assets/images/deuter.png" />
                </div>
                <div class="o-grid__cell o-grid__cell--width-33 u-letter-box--medium flex-box--center">
                    <img width="100" class="lazy-load o-image" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/placeholders/squares.svg" data-src="../wp-content/themes/gear-coop-blog/assets/images/olukai.png" />
                </div>
            </div>
        </div>
    </div>
</section>
<section class="post-content">
    <div class="u-window-box--xlarge u-centered u-bg-white">
        <h2 class="c-uppercase"><b>Checkout all of these inspiring submissions!</b></h2>
        <h4>Thank you to all who entered the LTVL contest.</h4>
    </div>
</section>
<section class="post-content">
    <?php
    $vanlife_args = array(
      'post_type' => 'vanlife'
    )
    ?>
    <?php $vanlife = new WP_Query( $vanlife_args ); ?>
    <?php if ( $vanlife->have_posts() ) : ?>
        <div class="o-container o-container--large u-letter-box--medium">
            <!--Grid-->
            <div class="o-grid o-grid--small-full o-grid--medium-full flex-wrap">
                <!--Col 1-->
                <?php while ( $vanlife->have_posts() ) : $vanlife->the_post(); ?>
                    <div class="o-grid__cell o-grid__cell--width-33@medium u-letter-box--medium card-hover">
                        <!--Card-->
                        <div class="c-card">
                            <div class="posts-thumb">
                                <a href="<?php echo esc_url( wp_get_shortlink()); ?>" data-lity></a>
                                <?php the_post_thumbnail( null, array(
                                      'class' => 'lazy-load o-image'
                                ) ); ?>
                            </div>
                            <div class="u-window-box--small">
                                <header class="c-card__header flex-box-column">
                                    <div class="u-centered">
                                        <a class="link-ltvl c-link--black u-window-box--xsmall" href="<?php the_field('ins_link'); ?>" target="blank">
                                          <i class="fa fa-instagram" aria-hidden="true"></i>
                                        </a>
                                        <a class="link-ltvl c-link--black u-window-box--xsmall" href="<?php the_field('fb_link'); ?>" target="blank">
                                          <i class="fa fa-facebook" aria-hidden="true"></i>
                                        </a>
                                        <a class="link-ltvl c-link--black u-window-box--xsmall" href="<?php the_field('tw_link'); ?>" target="blank">
                                          <i class="fa fa-twitter" aria-hidden="true"></i>
                                        </a>
                                        <a class="link-ltvl c-link--black u-window-box--xsmall" href="<?php the_field('www_link'); ?>" target="blank">
                                          <i class="fa fa-link" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    <a class="c-heading u-centered h2 h-link flex-item--bottom" href="<?php echo esc_url( wp_get_shortlink()); ?>" data-lity><?php the_title(); ?></a>
                                </header>
                                <div class="c-card__body u-centered posts-body">
                                    <?php echo get_excerpt(100); ?>
                                </div>
                                <footer class="c-card__footer c-card__footer--block u-centered">
                                    <a class="c-link c-uppercase" href="<?php echo esc_url( wp_get_shortlink()); ?>" data-lity><?php _e( 'Read More', 'cooper' ); ?></a>
                                </footer>
                            </div>
                        </div>
                        <!--Card-->
                    </div>
                <?php endwhile; ?>
                <!--Col 1-->
            </div>
            <?php
echo do_shortcode('[ajax_load_more container_type="div" css_classes="o-grid o-grid--small-full o-grid--medium-full flex-wrap" repeater="template_3" post_type="vanlife" posts_per_page="3" post_format="standard" category="'.$category->slug.'" cache="true" cache_id="cache-'.$category->slug.'" offset="6" pause="true" transition_container="false" images_loaded="true"]');
?>
            <!--Grid-->
        </div>
    <?php else : ?>
        <p><?php _e( 'Sorry, no posts matched your criteria.', 'cooper' ); ?></p>
    <?php endif; ?>
</section>

<?php get_footer(); ?>
