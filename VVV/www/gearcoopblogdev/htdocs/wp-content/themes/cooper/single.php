<?php
get_header(); ?>

            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <section class="post-header">
                        <header class="u-centered u-bg-white">
                            <div class="o-container featured-image">
                                <div class="o-container o-container--medium u-window-box--large u-absolute-center post-heading">
                                    <h4 class="h-inside-white"><?php the_category( ' | ' ); ?></h4>
                                    <h1 class="u-color-white"><?php the_title(); ?></h1>
                                </div>
                                <div class="featured-image-container u-absolute-center">
                                    <?php the_post_thumbnail( null, array(
                                          'class' => 'lazy-load o-image featured-image-img u-absolute-center'
                                    ) ); ?>
                                </div>
                            </div>
                        </header>
                    </section>
                <?php endwhile; ?>
            <?php endif; ?>
            <section class="u-bg-white post-content">
                <div class="o-container o-container--medium u-window-box--large">
                    <div class="o-grid">
                        <div class="o-grid__cell o-grid__cell--width-50 flex-box--center__left">
                            <h6 class="u-color-grey"><?php the_date(); ?></h6>
                        </div>
                        <div class="o-grid__cell o-grid__cell--width-50 flex-box--center__right post-author">
                            <!-- <img wp-get-avatar class="post-author-avatar" /> -->
                            <h6 class="u-color-grey h-inside-grey"><?php the_author_posts_link(); ?></h6>
                        </div>
                    </div>
                    <div class="u-window-box--medium">
                        <div>
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
            </section>
            <section class="u-bg-white post-products">
                <div class="o-container o-container--large u-window-box--large">
                    <h5 class="u-centered"><?php the_field('gear_title'); ?></h5>
                    <?php if( have_rows('products') ): ?>
                        <div class="o-grid o-grid--small-full o-grid--medium-full flex-wrap flex-center">
                            <?php while( have_rows('products') ): the_row(); ?>
                                <div class="o-grid__cell o-grid__cell--width-33@medium u-letter-box--medium u-centered card-hover">
                                    <div class="c-card post-product">
                                        <div class="u-window-box--small">
                                            <header class="c-card__header flex-box-column">
                                            <a href="<?php the_sub_field('product_link'); ?>" target="blank">
                                                    <?php $image = get_sub_field('product_img');  if( !empty($image) ): ?>
                                                    <img data-src="<?php echo $image['url']; ?>" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/placeholders/squares.svg" alt="<?php echo $image['alt']; ?>" width="100%" class="lazy-load o-image" />
                                                <?php endif; ?>
                                                </a>
                                                <a href="<?php the_sub_field('brand_link'); ?>" target="blank" class="flex-item--bottom"><h4 class="h-black-link"><?php the_sub_field('product_brand'); ?></h4></a>
                                            </header>
                                            <div class="c-card__body u-centered flex-box--center" style="display:<?php the_sub_field('showbody');?>;">
                                                <a href="<?php the_sub_field('product_link'); ?>" target="blank"><h3 class="h-black-link"><?php the_sub_field('product_title'); ?></h3></a>
                                            </div>
                                            <footer class="c-card__footer c-card__footer--block u-centered" style="display:<?php the_sub_field('showfooter');?>;">
                                                <div style="visibility:<?php the_sub_field('shopbtn');?>;">
                                                    <a href="<?php the_sub_field('product_link'); ?>" target="blank" class="c-button u-medium c-button--brand c-link--white"><?php _e( 'Shop Now', 'cooper' ); ?></a>
                                                </div>
                                            </footer>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </section>
            <section class="u-bg-white post-meta">
                <div class="o-container o-container--medium u-window-box--large u-centered">
                    <?php echo get_avatar( get_the_author_meta( 'ID' ) ); ?>
                    <h6><?php _e( 'Written by:', 'cooper' ); ?></h6>
                    <h5 class="h-inside-black"><?php the_author_posts_link(); ?></h5>
                    <?php the_author_meta( 'description' ); ?>
                    <div class="author-icons">
                        <?php
    $twitter_profile = get_the_author_meta( 'twitter_profile' );
    if ( $twitter_profile && $twitter_profile != '' ) {
      echo '<a href="' . esc_url($twitter_profile) . '" rel="author" target="blank" class="h6 h-black-link">Twitter</a>';
    }
    $facebook_profile = get_the_author_meta( 'facebook_profile' );
      if ( $facebook_profile && $facebook_profile != '' ) {
        echo '<a href="' . esc_url($facebook_profile) . '" rel="author" target="blank" class="h6 h-black-link">Facebook</a>';
      }
    $instagram_profile = get_the_author_meta( 'instagram_profile' );
      if ( $instagram_profile && $instagram_profile != '' ) {
        echo '<a href="' . esc_url($instagram_profile) . '" rel="author" target="blank" class="h6 h-black-link">Instagram</a>';
      }
    $website_profile = get_the_author_meta( 'website_profile' );
      if ( $website_profile && $website_profile != '' ) {
        echo '<a href="' . esc_url($website_profile) . '" rel="author" target="blank" class="h6 h-black-link">Website</a>';
      }
  ?>
                    </div>
                </div>
            </section>
            <section class="u-bg-grey-light post-related">
                <div class="o-container o-container--large u-letter-box--large">
                    <?php
wcr_related_posts(array(
   'taxonomy' => 'post_tag'
));
?>
                    <p class="text-xsmall u-centered"><?php the_tags(); ?></p>
                </div>
            </section>            

<?php get_footer(); ?>