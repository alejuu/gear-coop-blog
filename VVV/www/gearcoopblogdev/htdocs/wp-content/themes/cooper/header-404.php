<!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php wp_head(); ?>
    </head>
    <body>
        <!--Header-->
        <header class="main-header">
            <!--Main Nav-->
            <nav class="show-responsive show-medium--up">
                <ul class="c-nav c-nav--inline u-letter-box--medium c-uppercase">
                    <!--Logo-->
                    <li class="c-nav__content u-window-box--none">
                        <a href="https://www.gearcoop.com">
                            <img class="lazy-load o-image u-pillar-box--medium" data-src="/wp-content/themes/cooper/assets/Gear-Coop-logo-white.svg" data-src-mobile="/wp-content/themes/cooper/assets/Gear-Coop-logo.svg" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/placeholders/squares.svg" width="200">
                        </a>
                    </li>
                    <!--Logo-->
                    <!--Search-->
                    <li class="c-nav__item c-nav__item--right" data-anijs="if: click, do: visible, to: .search-form;if: click, do: input-search-full, to: .input-search">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </li>
                    <!--Search-->
                    <!--Shop-->
                    <li class="c-nav__item c-nav__item--right">
                        <button class="c-button c-button--brand nav-button">
                            <a href="https://www.gearcoop.com/" class="no-margin h4 c-link--white"><b><?php _e( 'Shop', 'cooper' ); ?></b></a>
                        </button>
                    </li>
                    <!--Nav Items-->
                    <li class="c-nav__items-container white-nav-container">
                        <?php
                            PG_Smart_Walker_Nav_Menu::$options['template'] = '<li class="c-nav__item c-nav__item--right h4 {CLASSES}" id="{ID}">
                                            <a {ATTRS}>{TITLE}</a>
                                          </li>';
                            PG_Smart_Walker_Nav_Menu::$options['current_class'] = 'c-nav__item--active';
                            wp_nav_menu( array(
                              'menu' => 'primary',
                              'menu_class' => 'c-nav__items',
                              'container' => '',
                              'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                              'fallback_cb' => 'wp_page_menu',
                              'walker' => new PG_Smart_Walker_Nav_Menu()
                        ) ); ?>
                    </li>
                    <!--Nav Items-->
                    <!--Shop-->
                </ul>
                <!--Search Form-->
                <?php get_search_form( true ); ?>
                <!--Search Form-->
            </nav>
            <!--Main Nav-->
            <!--Mobile Nav-->
            <nav class="hide-responsive hide-medium--up">
                <div class="c-nav--fixed mobile-logo">
                    <a href="https://www.gearcoop.com">
                        <img class="lazy-load" data-src="/wp-content/themes/cooper/assets/Gear-Coop-logo-small.svg" data-src-mobile="/wp-content/themes/cooper/assets/Gear-Coop-logo-small.svg" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/placeholders/squares.svg" width="35">
                    </a>
                </div>
                <!--Menu Icon-->
                <div class="burguer u-bg-black u-pillar-box--medium c-nav--fixed">
                    <i class="fa fa-bars u-bg-black u-color-white" aria-hidden="true" data-anijs="if: click, do: visible fade-in animated, to: .mobile-nav"></i>
                    <i class="fa fa-times u-bg-black u-color-white mobile-nav burguer-hide" aria-hidden="true" data-anijs="if: click, do: hide, to: .mobile-nav"></i>
                </div>
                <!--Menu Icon-->
                <!--Overlay-->
                <div class="c-overlay c-overlay--dismissable c-nav--fixed mobile-nav" data-anijs="if: click, do: hide, to: .mobile-nav"></div>
                <!--Overlay-->
                <div class="o-drawer u-highest o-drawer--right o-drawer--visible c-nav--fixed mobile-nav">
                    <?php
                        PG_Smart_Walker_Nav_Menu::$options['template'] = '<li class="c-nav__item h4 {CLASSES}" id="{ID}">
                                      <a {ATTRS}>{TITLE}</a>
                                    </li>';
                        PG_Smart_Walker_Nav_Menu::$options['current_class'] = 'c-nav__item--active';
                        wp_nav_menu( array(
                          'menu' => 'mobile',
                          'menu_class' => 'c-nav mobile-menu',
                          'container' => '',
                          'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                          'fallback_cb' => 'wp_page_menu',
                          'walker' => new PG_Smart_Walker_Nav_Menu_Mobile()
                    ) ); ?>
                    <form id="searchform" class="c-input-group search-form" method="get" action="<?php echo home_url('/'); ?>">
                        <div class="o-field">
                            <input class="c-field input-search" type="text" name="s" placeholder="Search Posts" value="<?php the_search_query(); ?>">
                        </div>
                        <button class="c-button c-button--brand" type="submit" value="Search">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </button>
                    </form>
                </div>
            </nav>
            <!--Mobile Nav-->
        </header>
        <!--Header-->
        <!--Main Content-->
        <main class="page-404">
