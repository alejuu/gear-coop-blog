<article class="post-article">
              <div class="col-md-4 col-post col-sm-6">
                <div class="eq-height">
                  <?php if ( has_post_thumbnail() ) { $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); } else { $feat_image = get_field('default_featured_image', 'option'); } ?>
                  <div id="ltvl-entry" class="thumb-image text-center" style="background-image: url(<?php echo $feat_image; ?>);">
                    <div class="ltvl-show">
                      <h3 class="text-uppercase ltvl-entry-title"><?php the_field('first_name'); ?></h3>
                      <h3 class="text-uppercase ltvl-entry-title"><?php the_field('last_name'); ?></h3>
                      <div class="ltvl-entry-content">
                        <?php the_excerpt(); ?>
                      </div>
                      <div class="ltvl-sm">
                        <a class="link-ltvl" href="<?php the_field('ins_link'); ?>" target="blank">
                          <img src="../wp-content/themes/gear-coop-blog/assets/images/i-black.png" alt="Instagram" width="32" class="ltvl-sm-icon"/>
                        </a>
                        <a class="link-ltvl" href="<?php the_field('fb_link'); ?>" target="blank">
                          <img src="../wp-content/themes/gear-coop-blog/assets/images/fb-black.png" alt="Facebook" width="32" class="ltvl-sm-icon"/>
                        </a>
                        <a class="link-ltvl" href="<?php the_field('tw_link'); ?>" target="blank">
                          <img src="../wp-content/themes/gear-coop-blog/assets/images/tw-black.png" alt="Twitter" width="32" class="ltvl-sm-icon"/>
                        </a>
                        <a class="link-ltvl" href="<?php the_field('www_link'); ?>" target="blank">
                          <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAaNJREFUeNp000kohGEcx/GZwQxJHORkudgSWbJkyZKIlIssR+ukOLhw4SQ3ipNwcScuLnIQkUmWCBdcJlG2XIgw+D76jd4Mb32aZ973+f+f7f/YbYFPBsqRhwK928YO1nBk7RxkaTswgAZEohA3eEQSnlGptgefJsiuYCdGEIJR9GnUUH1/0Wwm0I83DOHVoQ5udClzPlxYUGKn2i5986iv27+ENLRiGNkYxDWCUYpYhCELPbjCJOpxYhK04w4zuEQ6VpGihNGa8jHCMYYtpCLRjJKAWS3FbNAepvS/W7/+/zHqc4pltJlN3MAmblGkUVa0y2UKXNeGV+NJMzDJih2/asDuP55/nk/LyX33C1ZhLKpQzpCJccvxWZdgZneIJRVapEngRY0SnKNTa48zU1SgaV8gV0dqU4zXnMIDWkxRoBZ1GnlDFXmvNVdoT3yIQBWm/WvrVaImNKsazdMhNr1rVp8HxfzchQNEIR7zKNFmmdJ+VzElYw6N2FVCn/3XZerXbfRpvV59S1BQkDbdBH9YL9Nf19lcnhy929flCrjOXwIMANRqYeEhTz1jAAAAAElFTkSuQmCC" alt="Web Site" width="32" class="ltvl-sm-icon"/>
                        </a>
                      </div>
                      <button class="btn-apply" type="button" data-toggle="modal" data-target="#vanPost-<? the_ID(); ?>">Read More</button>
                    </div>
                  </div>
                </div>
              </div>
            </article>
            <div>
              <div id="vanPost-<? the_ID(); ?>" class="modal fade vanPost" tabindex="-1" role="dialog" aria-labelledby="vanPostLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">×</button>
                    </div>
                    <div class="modal-body">
                      <?php the_post_thumbnail( 'large', array( 'class' => 'img-vanpost' ) ); ?>
                            <div class="van-name">
                              <div>
                                <h3 class="text-uppercase ltvl-entry-title"><?php the_field('first_name'); ?></h3>
                                <h3 class="text-uppercase ltvl-entry-title"><?php the_field('last_name'); ?></h3>
                              </div>
                              <div>
                                <a class="link-ltvl" href="<?php the_field('ins_link'); ?>" target="blank">
                                  <img src="../wp-content/themes/gear-coop-blog/assets/images/i-black.png" alt="Instagram" width="32" class="ltvl-sm-icon"/>
                                </a>
                                <a class="link-ltvl" href="<?php the_field('fb_link'); ?>" target="blank">
                                  <img src="../wp-content/themes/gear-coop-blog/assets/images/fb-black.png" alt="Facebook" width="32" class="ltvl-sm-icon"/>
                                </a>
                                <a class="link-ltvl" href="<?php the_field('tw_link'); ?>" target="blank">
                                  <img src="../wp-content/themes/gear-coop-blog/assets/images/tw-black.png" alt="Twitter" width="32" class="ltvl-sm-icon"/>
                                </a>
                                <a class="link-ltvl" href="<?php the_field('www_link'); ?>" target="blank">
                                  <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAaNJREFUeNp000kohGEcx/GZwQxJHORkudgSWbJkyZKIlIssR+ukOLhw4SQ3ipNwcScuLnIQkUmWCBdcJlG2XIgw+D76jd4Mb32aZ973+f+f7f/YbYFPBsqRhwK928YO1nBk7RxkaTswgAZEohA3eEQSnlGptgefJsiuYCdGEIJR9GnUUH1/0Wwm0I83DOHVoQ5udClzPlxYUGKn2i5986iv27+ENLRiGNkYxDWCUYpYhCELPbjCJOpxYhK04w4zuEQ6VpGihNGa8jHCMYYtpCLRjJKAWS3FbNAepvS/W7/+/zHqc4pltJlN3MAmblGkUVa0y2UKXNeGV+NJMzDJih2/asDuP55/nk/LyX33C1ZhLKpQzpCJccvxWZdgZneIJRVapEngRY0SnKNTa48zU1SgaV8gV0dqU4zXnMIDWkxRoBZ1GnlDFXmvNVdoT3yIQBWm/WvrVaImNKsazdMhNr1rVp8HxfzchQNEIR7zKNFmmdJ+VzElYw6N2FVCn/3XZerXbfRpvV59S1BQkDbdBH9YL9Nf19lcnhy929flCrjOXwIMANRqYeEhTz1jAAAAAElFTkSuQmCC" alt="Web Site" width="32" class="ltvl-sm-icon"/>
                                </a>
                              </div>
                            </div>
                            <div class="van-content">
                              <?php the_content();?>
                            </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-cancel" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>