<article class="post-article">
                            <div class="col-md-4 col-post col-sm-6"> 
                                <div class="eq-height">
                                    <a href="<?php echo esc_url( wp_get_shortlink()); ?>"> 
                                         <?php if ( has_post_thumbnail() ) { $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'small' ); } else { $feat_image = get_field('default_featured_image', 'option'); } ?>
                                        <div class="thumb-image" style="background-image: url(<?php echo $feat_image; ?>);"></div>                                                     
                                    </a>
                                    <a href="<?php echo esc_url( wp_get_shortlink()); ?>"><h3 class="post-title h-link text-uppercase"><?php the_title(); ?></h3></a>
                                    <p class="post-date"><?php echo get_the_time( get_option( 'date_format' ) ) ?></p>
                                    <p class="post-author"> <?php _e( '| Words by', 'gearcoopblogtheme' ); ?> <a href="http://" class="post-author-link"><?php the_author_posts_link(); ?></a></p>
                                    <p class="post-excerpt"><?php the_excerpt( ); ?></p>
                                    <a href="<?php echo esc_url( wp_get_shortlink()); ?>" class="post-read-more-link"><p class="post-read-more"><?php _e( 'Read More', 'gearcoopblogtheme' ); ?></p></a>
                                </div>                                                                                                                                                                                                                                                                              
                            </div>                                                                                  
                        </article>